### Changelog 
# CHANGELOG Listas de especies

## [**1.1.0**] - (proximamente)

### Agregado
- Descargas de listas de especies.

#### Actualizado
- Actualizaciones de la interfaz visual.
- Mejoras de usabilidad y experiencia de usuario.
#### Solucionado
- Conteo del *Home*.

#### Eliminado
- Filtro de ubicación.
- Barra de búsqueda superior.

## [**1.0.0**] – (2018-06)
### Agregado
- sección de búsqueda por listas de especies:
    - Filtros:
        - Taxonomía
        - Nombre del recurso
        - Publicador
        - Redes y proyectos
        - Licencia
- Sección de búsqueda por publicadores:
Lista todos los recursos publicados a través del SiB Colombia.
    - Filtros:
        - Publicador
        - Redes y proyectos
- Conexión con nuevo API de consultas.
- Conexión con nuevo API de búsquedas.
