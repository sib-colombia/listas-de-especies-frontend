import fetch from 'isomorphic-fetch';

import { URL_API_USERS } from '../config/const';

export function getPage(id) {
  return fetch(`${URL_API_USERS}/api/static/get/listas_${id}`, { method: 'GET' }).then((response) => {
    return response.json();
  }).then((data) => {
    return data;
  })
}
