import fetch from 'isomorphic-fetch';

import { URL_ES, URL_API, KEY_TYPE, THUMBS_API } from '../config/const';
import { request } from '../util'

const xhr = new XMLHttpRequest()

export function getOccurrenceList(offset, search) {
  if (search && search.indexOf('?') >= 0) {
    search = search.slice(1)
  }

  return request(xhr, 'GET', `${URL_API}/api/resource/searchlite?keyType=${KEY_TYPE}&page=${offset}${search && '&' + search}`)
}
export function ESgetOccurrenceList(offset, search) {
  if (search.indexOf('?') >= 0) {
    search = search.slice(1)
  }

  return request(xhr, 'GET', `${URL_ES}/api/search?keyType=${KEY_TYPE}&page=${offset}${search && '&' + search}`)
}

export function descargar(search, usuario) {
  if (search.indexOf('?') >= 0) {
    search = search.slice(1)
  }

  return fetch(`${URL_API}/api/search/download?keyType=${KEY_TYPE}&email=${usuario}${search && '&' + search}`, {
    method: 'GET'
  }).then((response) => {
    return response.json()
  }).then((data) => {
    return data
  });
}

export function getOccurrence(id) {
  return fetch(`${URL_API}/api/resource/id?occurrenceID=` + id, {
    method: 'GET'
  }).then((response) => {
    return response.json()
  }).then((data) => {
    return data
  })
}

export function getThumb(img) {
  return `${THUMBS_API}/` + encodeURIComponent(img)
}



export function getImagesList() {
  return fetch(`${URL_API}/api/specimens/read`, { method: 'GET' }).then((response) => {
    return response.json()
  }).then((data) => {
    return data
  });
}

/*

export function getOccurrence(id) {
  return fetch(`${URL_API}/api/biological/basic/` + id, { method: 'GET' }).then((response) => {
    return response.json()
  }).then((data) => {
    return data
  })

}

export function getFacets() {
  return fetch(`${URL_API}/api/biological/filters`, { method: 'GET' }).then((response) => {
    return response.json()
  }).then((data) => {
    return data
  })
}

export function getFacetsLumon() {
  return fetch(`${URL_API}/api/lumon/resource/facets?facet[]=habitat&facet[]=taxonRank`, { method: 'GET' }).then((response) => {
    return response.json()
  }).then((data) => {
    return data
  })
}

*/


