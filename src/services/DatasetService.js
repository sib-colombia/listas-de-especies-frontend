import fetch from 'isomorphic-fetch';

import { URL_MONGO, URL_ES, URL_API, URL_API_IDE, KEY_TYPE } from '../config/const';
import { request } from '../util'

const xhr = new XMLHttpRequest()

export function getDatasetList(page, search, orderBy) {
  if (search && search.indexOf('?') >= 0) {
    search = search.slice(1)
  }

  return request(xhr, 'GET', `${URL_ES}/api/agg?agg=gbifId&keyType=${KEY_TYPE}&page=${page}${search && '&' + search}`)
//  return request(xhr, 'GET', `${URL_API}/api/resource/list?keyType=${KEY_TYPE}&page=${page}${search && '&' + search}${orderBy ? '&ord=' + orderBy : ''}`)
}

export function getDataset(id) {
  return fetch(`${URL_MONGO}/api/dataset/${id}`, { method: 'GET' }).then((response) => {
    return response.json()
  }).then((data) => {
    return data
  })
}

export function getUrlData(url) {
  return `${URL_API_IDE}/api/dwc/data/`+encodeURIComponent(url)
}
