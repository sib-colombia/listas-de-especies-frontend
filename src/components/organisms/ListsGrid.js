import React, { Component } from 'react';
import map from 'lodash/map';

import ListsGridItem from '../molecules/ListsGridItem';

class ListsGrid extends Component {
  render() {
    const {data} = this.props;
    return (
      <div className="uk-container uk-container-small">
        <div className="uk-child-width-1-1 uk-child-width-1-3@s uk-grid-small uk-grid-match" data-uk-grid>
          {data && map(data, (dataset, key) => (<ListsGridItem key={key} data={dataset}/>))}
        </div>
      </div>
    );
  }
}

export default ListsGrid;
