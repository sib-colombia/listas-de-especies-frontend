import React, { Component } from 'react';
import _ from 'lodash';

import SpeciesRow from '../molecules/SpeciesRow';
import Pagination from '../atoms/Pagination';
import * as OccurrenceService from '../../services/OccurrenceService';
import Loading from '../atoms/Loading';

class SpeciesTable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      count: NaN,
    }
  }

  componentDidMount() {
    this.offsetResults(0);
  }

  onChangePage(pageOfItems) {
    const page = pageOfItems;
    this.offsetResults(page, pageOfItems);
  }

  offsetResults(offset, pageOfItems) {
    //console.log("Resultados especie")
    //console.log(this.props.busqueda)
    OccurrenceService.ESgetOccurrenceList(offset, this.props.busqueda).then(data => {
        //console.log("data: -> ")
        //console.log(data)
        this.setState({
          data: data.hits.hits,
          offset: offset,
          count: data.hits.total,
          currentPage: pageOfItems
        })

      // this.props.species(data.count);
    });
  }

  render() {
    return (
      <li>
        {(this.state.data && <div className="uk-card uk-card-default uk-overflow-auto uk-margin">
          <table className="uk-table uk-table-divider uk-table-small uk-table-hover">
            <thead>
              <tr>
                {/*<th>Enlaces</th>*/}
                <th>Nombre cientifico</th>
                <th>Reino</th>
                <th>Filo</th>
                <th>Clase</th>
                <th>Orden</th>
                <th>Familia</th>
                <th>Género</th>
                <th className="uk-text-nowrap">Epíteto específico</th>
                <th className="uk-text-nowrap">Categoría taxonómica</th>
                <th>Nombre común</th>
                <th>Estado de amenaza</th>
                <th>Apéndice CITES</th>
              </tr>
            </thead>
            <tbody>
              {_.map(this.state.data, (specie, key) => (<SpeciesRow key={key} data={specie} />))}
            </tbody>
          </table>
        </div>) || <Loading />}
        {
          this.state.data && <Pagination items={this.state.count} onChangePage={(number) => {
            this.onChangePage(number - 1);
          }} />
        }
      </li>
    );
  }
}

export default SpeciesTable;
