import React, {Component} from 'react';
import map from 'lodash/map';

import ListsListItem from '../molecules/ListsListItem';
import Loading from '../atoms/Loading';

class ListsList extends Component {
  render() {
    const {data} = this.props;
    return (<div className="uk-container uk-container-small">
      {(data && <div className="uk-flex uk-flex-column">
        {map(data, (dataset, key) => (<ListsListItem key={key} data={dataset}/>))}
      </div>) || <Loading />}
    </div>)
  }
}

export default ListsList;
