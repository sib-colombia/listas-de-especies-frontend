import React, { Component } from 'react';
import map from 'lodash/map';

import ProvidersGridItem from '../molecules/ProvidersGridItem';

class ProvidersGrid extends Component {
  render() {
    const { data } = this.props;
    return (
      <div className="uk-container uk-container-small">
        <div className="uk-child-width-1-1 uk-child-width-1-3@s uk-grid-small uk-grid-match" data-uk-grid>
          {data && map(data, (dataset, key) => (<ProvidersGridItem key={key} data={dataset}/>))}
        </div>
      </div>
    )
  }
}

export default ProvidersGrid;
