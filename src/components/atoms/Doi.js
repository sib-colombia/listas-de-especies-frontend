import React, { Component } from 'react';

class Doi extends Component {
  render() {
    const { label } = this.props
    console.log("infomacion  " + this.props)
    let l = ""
    if (label!==undefined && label!==null){
      l = label.substr(16)
    }else{
      l = "No Encontrado"
    }
    
    
    return (
      <a className="uk-link-reset" href={`https://doi.org/${l.substr(0)}`} target="_blank" >
        <div className="uk-flex uk-flex-center uk-flex-middle uk-text-bold uk-background-tertiary uk-grid-collapse uk-child-width-1-1 uk-child-width-auto@s" data-uk-grid>
          <div>
            <span className="uk-text-default uk-margin-small-left uk-margin-small-right">DOI</span>
          </div>
          <div className="uk-background-default uk-text-center uk-width-expand" style={{ border: 'solid 2px #00aab6', padding: 1.5 }}>
          <span className="uk-text-small uk-text-break">{l.substr(0)}</span>
          </div>
        </div>
      </a>
    );
  }
}

export default Doi;
