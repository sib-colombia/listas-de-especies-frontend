import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import NumberFormat from 'react-number-format';
import map from 'lodash/map';

import Doi from '../atoms/Doi';
import Reference from '../atoms/Reference';

import { getKeyword } from '../../util';

class ListsListItem extends Component {

  render() {
    const { data } = this.props;
    let x = data.platform.hits.hits[0]._source

    
    return (<div className="uk-heading-divider uk-padding-small">
      <div className="uk-child-width-1-1 uk-child-width-expand@s" data-uk-grid="data-uk-grid">
        <div className="uk-width-1-4">
          <Link className="uk-link-reset uk-inline" to={`/dataset/${data.key}`}>
            <img alt="" className="uk-width-1-1" src={x.resourceLogoUrl} />
          </Link>
        </div>
        
        <div>
          <h3 className="uk-text-tertiary uk-text-truncate">
            <Link className="uk-link-reset" to={`/list/${data.key}`}>{x.titleResource}</Link>
          </h3>
          <p className="uk-heading-divider box-truncate text-ellipsis" >{x.abstract && (x.abstract)}</p>
          <Link to={`/list/${data.key}`} className="uk-text-tertiary" style={{
            textDecoration: 'none'
          }}>{data.organizationName}</Link>
          <div className="uk-child-width-1-2 uk-child-width-1-2@s uk-child-width-1-3@m uk-grid-small uk-margin-small-top uk-flex uk-flex-bottom" data-uk-grid="data-uk-grid">
            {getKeyword(data.keywordCollections, "Taxonomic Authority") && <div><Reference kw={data.keywordCollections} /></div>}
            
            <div><Doi label={x.doi} /></div>
            <div>
              <div className="uk-background-tertiary">
                <Link className="uk-link-reset" to={`/list/${data.key}`}>
                  <div className="uk-text-default uk-text-center uk-text-bold uk-margin-small-left uk-margin-small-right" style={{
                    padding: 3.5
                  }}>
                    <span><NumberFormat value={data.doc_count} displayType="text" thousandSeparator /></span>
                    <span className="uk-text-small"> Taxa</span>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>);
  }
}

export default ListsListItem;
