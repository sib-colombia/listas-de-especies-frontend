import React, { Component } from 'react';
import { union, indexOf, fill, map } from 'lodash';

class ExtensionTable extends Component {


  render() {
    const { titulo, data } = this.props;
    
    if(data===undefined || data===null) return <div/>;
    
    let traduccion = {
"locationID": "ID de la ubicación", 
"locality": "Localidad", 
"countryCode": "Código del país", 
"lifeStage": "Etapa de vida", 
"occurrenceStatus": "Estado del registro biológico", 
"threatStatus": "Estado de amenaza", 
"establishmentMeans": "Medios de establecimiento", 
"appendixCITES": "Apéndices CITES", 
"eventDate": "Fecha del evento", 
"startDayOfYear": "Día inicial del año", 
"endDayOfYear": "Día final del año", 
"source": "Fuente", 
"occurrenceRemarks": "Comentarios del registro biológico", 
"datasetID": "ID del conjunto de datos", 
"isMarine": "Es marino", 
"isFreshwater": "Es de agua dulce", 
"isTerrestrial": "Es terrestre", 
"isInvasive": "Es Invasor", 
"isHybrid": "Es híbrido", 
"isExtinct": "Extinto", 
"livingPeriod": "Periodo de vida", 
"ageInDays": "Edad en días", 
"sizeInMillimeters": "Tamaño en milímetros", 
"massInGrams": "Peso en gramos", 
"lifeForm": "Forma de vida", 
"habitat": "Hábitat", 
"sex": "Sexo", 
"datasetID": "ID del conjunto de datos", 
"typeStatus": "Estado del Tipo", 
"typeDesignationType": "Designación del Tipo", 
"typeDesignatedBy": "Tipo designado por", 
"scientificName": "Nombre científico", 
"taxonRank": "Categoría del taxón", 
"bibliographicCitation": "Citación bibliográfica", 
"occurrenceID": "ID del registro biológico", 
"institutionCode": "Código de la institución", 
"collectionCode": "Código de la colección", 
"catalogNumber": "Número de catálogo", 
"locality": "Localidad", 
"sex": "Sexo", 
"recordedBy": "Registrado por", 
"source": "Fuente", 
"verbatimEventDate": "Fecha original del evento", 
"verbatimLabel": "Etiqueta original", 
"verbatimLongitude": "Longitud original", 
"verbatimLatitude": "Latitud original", 
"datasetID": "ID del conjunto de datos", 
"identifier": "Identificador de la referencia", 
"title": "Título", 
"creator": "Autor", 
"date": "Fecha", 
"source": "Fuente", 
"description": "Resumen", 
"subject": "Palabras clave", 
"language": "Idioma", 
"rights": "Derechos de autor", 
"type": "Tipo de publicación", 
"bibliographicCitation": "Referencia completa", 
"taxonRemarks": "Comentarios del taxón", 
"datasetID": "ID del conjunto de datos", 
"type": "Tipo", 
"format": "Formato", 
"identifier": "Identificador", 
"references": "Referencias", 
"title": "Título", 
"description": "Descripción", 
"created": "Fecha de creación", 
"creator": "Autor", 
"contributor": "Parte asociada", 
"publisher": "Editor", 
"audience": "Audiencia", 
"source": "Fuente", 
"license": "Licencia", 
"rightsHolder": "Titular de los derechos", 
"datasetID": "ID del conjunto de datos", 
"description": "Descripción", 
"type": "Tipo", 
"source": "Fuente", 
"language": "Idioma", 
"created": "Fecha de creación", 
"creator": "Autor", 
"contributor": "Parte asociada", 
"audience": "Audiencia", 
"license": "Licencia", 
"rightsHolder": "Titular de los derechos", 
"datasetID": "ID del conjunto de datos", 
"vernacularName": "Nombres comunes", 
"source": "Fuente", 
"language": "Idioma", 
"temporal": "Contexto temporal", 
"locationID": "ID de la ubicación", 
"locality": "Localidad", 
"countryCode": "Código del país", 
"sex": "Sexo", 
"lifeStage": "Etapa de vida", 
"isPlural": "Nombre en plural", 
"isPreferredName": "Nombre preferido", 
"organismPart": "Parte del organismo", 
"taxonRemarks": "Comentarios del taxón", 
"datasetID": "ID del conjunto de datos", 
"occurrenceID": "ID del registro biológico", 
"basisOfRecord": "Base del registro", 
"institutionCode": "Código de la institución", 
"collectionCode": "Código de la colección", 
"catalogNumber": "Número de catálogo", 
"type": "Tipo", 
"modified": "Modificado", 
"language": "Idioma", 
"license": "Licencia", 
"rightsHolder": "Titular de los derechos", 
"accessRights": "Derechos de acceso", 
"bibliographicCitation": "Citación bibliográfica", 
"references": "Referencias", 
"institutionID": "ID de la institución", 
"collectionID": "ID de la colección", 
"datasetID": "ID del conjunto de datos", 
"datasetName": "Nombre del conjunto de datos", 
"ownerInstitutionCode": "Código de la institución propietaria", 
"informationWithheld": "Información retenida", 
"dataGeneralizations": "Generalización de los datos", 
"dynamicProperties": "Propiedades dinámicas", 
"occurrenceRemarks": "Comentarios del registro biológico", 
"recordNumber": "Número de registro", 
"recordedBy": "Registrado por", 
"organismID": "ID del individuo", 
"individualCount": "Número de individuos", 
"organismQuantity": "Cantidad del Organismo", 
"organismQuantityType": "Tipo de Cantidad del Organismo", 
"organismName": "Nombre del organismo", 
"organismScope": "Alcance del organismo", 
"associatedOrganisms": "Organismos asociados", 
"organismRemarks": "Comentarios del organismo", 
"sex": "Sexo", 
"lifeStage": "Etapa de vida", 
"reproductiveCondition": "Condición reproductiva", 
"behavior": "Comportamiento", 
"establishmentMeans": "Medios de establecimiento", 
"occurrenceStatus": "Estado del registro biológico", 
"preparations": "Preparaciones", 
"disposition": "Disposición", 
"otherCatalogNumbers": "Otros números de catálogo", 
"previousIdentifications": "Identificaciones previas", 
"associatedMedia": "Medios asociados", 
"associatedReferences": "Referencias asociadas", 
"associatedOccurrences": "Registros biológicos asociados", 
"associatedSequences": "Secuencias asociadas", 
"associatedTaxa": "Taxones asociados", 
"materialSampleID": "ID de muestra del ejemplar", 
"eventID": "ID del evento", 
"parentEventID": "ID del evento parental", 
"samplingProtocol": "Protocolo de muestreo", 
"sampleSizeValue": "Tamaño de la muestra", 
"sampleSizeUnit": "Unidad del tamaño", 
"samplingEffort": "Esfuerzo de muestreo", 
"eventDate": "Fecha del evento", 
"eventTime": "Hora del evento", 
"startDayOfYear": "Día inicial del año", 
"endDayOfYear": "Día final del año", 
"year": "Año", 
"month": "Mes", 
"day": "Día", 
"verbatimEventDate": "Fecha original del evento", 
"habitat": "Hábitat", 
"fieldNumber": "Número de campo", 
"fieldNotes": "Notas de campo", 
"eventRemarks": "Comentarios del evento", 
"locationID": "ID de la ubicación", 
"higherGeographyID": "ID de la geografía superior", 
"higherGeography": "Geografía superior", 
"continent": "Continente", 
"waterBody": "Cuerpo de agua", 
"islandGroup": "Grupo de islas", 
"island": "Isla", 
"country": "País", 
"countryCode": "Código del país", 
"stateProvince": "Departamento", 
"county": "Municipio", 
"municipality": "Centro poblado / Cabecera municipal", 
"locality": "Localidad", 
"verbatimLocality": "Localidad original", 
"verbatimElevation": "Elevación original", 
"minimumElevationInMeters": "Elevación mínima en metros", 
"maximumElevationInMeters": "Elevación máxima en metros", 
"verbatimDepth": "Profundidad original", 
"minimumDepthInMeters": "Profundidad mínima en metros", 
"maximumDepthInMeters": "Profundidad máxima en metros", 
"minimumDistanceAboveSurfaceInMeters": "Distancia mínima de la superficie metros", 
"maximumDistanceAboveSurfaceInMeters": "Distancia máxima de la superficie metros", 
"locationAccordingTo": "Ubicación de acuerdo con", 
"locationRemarks": "Comentarios de la ubicación", 
"verbatimCoordinates": "Coordenadas originales", 
"verbatimLatitude": "Latitud original", 
"verbatimLongitude": "Longitud original", 
"verbatimCoordinateSystem": "Sistema original de coordenadas", 
"verbatimSRS": "SRS original", 
"decimalLatitude": "Latitud decimal", 
"decimalLongitude": "Longitud decimal", 
"geodeticDatum": "Datum geodésico", 
"coordinateUncertaintyInMeters": "Incertidumbre de las coordenadas en metros", 
"coordinatePrecision": "Precisión de las coordenadas", 
"pointRadiusSpatialFit": "Ajuste espacial del radio-punto", 
"footprintWKT": "WKT footprint", 
"footprintSRS": "SRS footprint", 
"footprintSpatialFit": "Ajuste espacial de footprint", 
"georeferencedBy": "Georreferenciado por", 
"georeferencedDate": "Fecha de georreferenciación", 
"georeferenceProtocol": "Protocolo de georreferenciación", 
"georeferenceSources": "Fuentes de georreferenciación", 
"georeferenceVerificationStatus": "Estado de la verificación de la georreferenciación", 
"georeferenceRemarks": "Comentarios de la georreferenciación", 
"geologicalContextID": "ID del contexto geológico", 
"earliestEonOrLowestEonothem": "Eón temprano o eonotema inferior", 
"latestEonOrHighestEonothem": "Eón tardío o eonotema superior", 
"earliestEraOrLowestErathem": "Era temprana o eratema inferior", 
"latestEraOrHighestErathem": "Era tardía o eratema superior", 
"earliestPeriodOrLowestSystem": "Periodo temprano o sistema inferior", 
"latestPeriodOrHighestSystem": "Periodo tardío o sistema superior", 
"earliestEpochOrLowestSeries": "Época temprana o serie inferior", 
"latestEpochOrHighestSeries": "Época tardía o serie superior", 
"earliestAgeOrLowestStage": "Edad temprana o piso inferior", 
"latestAgeOrHighestStage": "Edad tardía o piso superior", 
"lowestBiostratigraphicZone": "Zona bioestratigráfica inferior", 
"highestBiostratigraphicZone": "Zona bioestratigráfica superior", 
"lithostratigraphicTerms": "Términos litoestratigráficos", 
"group": "Grupo", 
"formation": "Formación", 
"member": "Miembro", 
"bed": "Capa", 
"identificationID": "ID de la identificación", 
"identifiedBy": "Identificado por", 
"dateIdentified": "Fecha de identificación", 
"identificationReferences": "Referencias de la identificación", 
"identificationVerificationStatus": "Estado de la verificación de la identificación", 
"identificationRemarks": "Comentarios de la Identificación", 
"identificationQualifier": "Calificador de la identificación", 
"typeStatus": "Estado del tipo", 
"taxon": "ID del taxón", 
"scientificNameID": "ID del nombre científico", 
"acceptedNameUsageID": "ID del nombre aceptado usado", 
"parentNameUsageID": "ID del nombre parental usado", 
"originalNameUsageID": "ID del nombre original usado", 
"nameAccordingToID": "ID del nombre de acuerdo con", 
"namePublishedInID": "ID del nombre publicado en", 
"taxonConceptID": "ID del concepto del taxón", 
"scientificName": "Nombre científico", 
"acceptedNameUsage": "Nombre aceptado usado", 
"parentNameUsage": "Nombre parental usado", 
"originalNameUsage": "Nombre original usado", 
"nameAccordingTo": "Nombre de acuerdo con", 
"namePublishedIn": "Nombre publicado en", 
"namePublishedInYear": "Nombre publicado en el año", 
"higherClassification": "Clasificación superior", 
"kingdom": "Reino", 
"phylum": "Filo", 
"class": "Clase", 
"order": "Orden", 
"family": "Familia", 
"genus": "Género", 
"subgenus": "Subgénero", 
"specificEpithet": "Epíteto específico", 
"infraspecificEpithet": "Epíteto infraespecífico", 
"taxonRank": "Categoría del taxón", 
"verbatimTaxonRank": "Categoría original del taxón", 
"scientificNameAuthorship": "Autoría del nombre científico", 
"vernacularName": "Nombre común", 
"nomenclaturalCode": "Código nomenclatural", 
"taxonomicStatus": "Estado taxonómico", 
"nomenclaturalStatus": "Estado nomenclatural", 
"taxonRemarks": "Comentarios del taxón", 
"id": "ID del registro biológico", 
"measurementID": "Identificador", 
"measurementType": "Tipo", 
"measurementValue": "Valor", 
"measurementAccuracy": "Precisión", 
"measurementUnit": "Unidad de medida", 
"measurementDeterminedBy": "Determinado por", 
"measurementDeterminedDate": "Fecha", 
"measurementMethod": "Método", 
"measurementRemarks": "Comentarios", 
"id": "ID del registro biológico", 
"resourceRelationshipID": "ID de relación", 
"resourceID ": "ID del recurso", 
"relatedResourceID": "ID del recurso relacionado", 
"relationshipOfResource": "Relación de recursos", 
"relationshipAccordingTo": "Relación de acuerdo a", 
"relationshipEstablishedDate": "Fecha de relación", 
"relationshipRemarks": "Comentarios"}
    let header;
    let body = [];
    map(data, (value, key) => {
      let ks = Object.keys(value)
      if (ks.indexOf("hasLocation")!==-1)
        ks.splice(ks.indexOf("hasLocation"), 1);
      if (ks.indexOf("id")!==-1)
        ks.splice(ks.indexOf("id"), 1);
      header = union(header, ks)
      body.push(fill(Array(header.length), ''))
      map(value, (v, k) => {
        body[key][indexOf(header, k)] = v;
      })
    })
  
    return (
      <div>
        <h3>{titulo}</h3>

        <div className="uk-overflow-auto uk-margin-top">
          <table className="uk-card uk-card-default uk-table uk-table-divider uk-table-small">
            <thead>
              <tr>
                {
                  map(header, (title, key) => (
                    <th key={key} className="uk-width-small">{traduccion[title]}</th>
                  ))
                }
              </tr>
            </thead>
            <tbody>
              {
                map(body, (value, key) => (
                  <tr key={key}>
                    {
                      map(value, (v, k) => 
                        {
                        if(typeof v === 'string')
                          return <td key={k} className="uk-text-break">
                            {
                              v && typeof v === 'string' &&
                              v.match(/^http.*$/) ? <a href={v} target="_blank">{v}</a> : v
                            }
                          </td>
                        else
                          return <td key={k} className="uk-text-break"></td>
                        }
                      )
                    }
                  </tr>
                ))
              }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default ExtensionTable;
