import React, { Component } from 'react';

class ExemplaryType extends Component {
  render() {
    // const { data } = this.props;
    
    return (
      <div className="uk-grid-collapse uk-margin-small-top uk-padding-small uk-child-width-1-1 uk-striped" data-uk-grid="data-uk-grid">
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Estado del Tipo</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Designación del tipo</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Tipo designado por</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Nombre científico</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Categoría del taxón</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Citación bibliográfica</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">ID del registro biológico</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Código de la institución</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Código de la colección</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Número de catálogo</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ExemplaryType;
