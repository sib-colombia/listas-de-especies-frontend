import React, { Component } from 'react';

class References extends Component {
  render() {
    // const { data } = this.props;
    
    return (
      <div className="uk-grid-collapse uk-margin-small-top uk-padding-small uk-child-width-1-1 uk-striped" data-uk-grid="data-uk-grid">
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Identificador de la referencia</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Título</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Autor</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Fecha</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Fuente</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Resumen</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Palabras clave</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Idioma</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Derechos de autor</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Tipo de publicación</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default References;
