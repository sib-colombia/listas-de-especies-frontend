import React, { Component } from 'react';

class Distribution extends Component {
  render() {
    // const { data } = this.props;
    
    return (
      <div className="uk-grid-collapse uk-margin-small-top uk-padding-small uk-child-width-1-1 uk-striped" data-uk-grid="data-uk-grid">
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">ID de la ubicación</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Localidad</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Código del país</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Etapa de vida</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Estado del registro biológico</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Estado de amenaza</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Medios de establecimiento</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Apéndice CITES</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Fecha del evento</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Número de catálogo</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Distribution;
