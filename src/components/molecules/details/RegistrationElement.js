import React, { Component } from 'react';

class RegistrationElement extends Component {
  render() {
    // const { data } = this.props;
    
    return (
      <div className="uk-grid-collapse uk-margin-small-top uk-padding-small uk-child-width-1-1 uk-striped" data-uk-grid="data-uk-grid">
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Idioma</span>
            </div>
            <div>
              <span className="uk-text-break">Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Modificado</span>
            </div>
            <div>
              <span className="uk-text-break">Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Derechos</span>
            </div>
            <div>
              <span className="uk-text-break">Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Titular de los derechos</span>
            </div>
            <div>
              <span className="uk-text-break">Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Derechos de acceso</span>
            </div>
            <div>
              <span className="uk-text-break">Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Modificado</span>
            </div>
            <div>
              <span className="uk-text-break">Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Derechos</span>
            </div>
            <div>
              <span className="uk-text-break">Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Titular de los derechos</span>
            </div>
            <div>
              <span className="uk-text-break">Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Idioma</span>
            </div>
            <div>
              <span className="uk-text-break">Contenido relacionado</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RegistrationElement;
