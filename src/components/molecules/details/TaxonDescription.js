import React, { Component } from 'react';


class TaxonDescription extends Component {
  render() {
    // const { data } = this.props;

    return (
      <div className="uk-grid-collapse uk-margin-small-top uk-padding-small uk-child-width-1-1 uk-striped" data-uk-grid="data-uk-grid">
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Descripción</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Tipo</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Fuente</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Idioma</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Fecha de creación</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Autor</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Parte asociada</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Audiencia</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Licencia</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="data-uk-grid">
            <div>
              <span className="uk-text-bold">Titular de los derechos</span>
            </div>
            <div>
              <span>Contenido relacionado</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TaxonDescription;
