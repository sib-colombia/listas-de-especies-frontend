import React, {Component} from 'react';

class Taxon extends Component {
  render() {
    const { data } = this.props;
    
    return (<div className="uk-grid-collapse uk-margin-small-top uk-padding-small uk-child-width-1-1 uk-striped" data-uk-grid="">
      {
        (data.taxonID && data.taxonID !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">ID del taxón</span>
              </div>
              <div>
                <span>{data.taxonID}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.scientificNameID && data.scientificNameID !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">ID del nombre científico</span>
              </div>
              <div>
                <span>{data.scientificNameID}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.acceptedNameUsageID && data.acceptedNameUsageID !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">ID del nombre aceptado usado</span>
              </div>
              <div>
                <span>{data.acceptedNameUsageID}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.parentNameUsageID && data.parentNameUsageID !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">ID del nombre parental usado</span>
              </div>
              <div>
                <span>{data.parentNameUsageID}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.originalNameUsageID && data.originalNameUsageID !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">ID del nombre original usado</span>
              </div>
              <div>
                <span>{data.originalNameUsageID}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.nameAccordingToID && data.nameAccordingToID !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">ID del nombre de acuerdo con</span>
              </div>
              <div>
                <span>{data.nameAccordingToID}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.namePublishedInID && data.namePublishedInID !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">ID del nombre publicado en</span>
              </div>
              <div>
                <span>{data.namePublishedInID}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.taxonConceptID && data.taxonConceptID !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">ID del concepto del taxón</span>
              </div>
              <div>
                <span>{data.taxonConceptID}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.scientificName && data.scientificName !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Nombre científico</span>
              </div>
              <div>
                <span>{data.scientificName}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.acceptedNameUsage && data.acceptedNameUsage !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Nombre aceptado usado</span>
              </div>
              <div>
                <span>{data.acceptedNameUsage}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.parentNameUsage && data.parentNameUsage !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Nombre parental usado</span>
              </div>
              <div>
                <span>{data.parentNameUsage}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.originalNameUsage && data.originalNameUsage !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Nombre original usado</span>
              </div>
              <div>
                <span>{data.originalNameUsage}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.nameAccordingTo && data.nameAccordingTo !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Nombre de acuerdo con</span>
              </div>
              <div>
                <span>{data.nameAccordingTo}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.namePublishedIn && data.namePublishedIn !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Nombre publicado en</span>
              </div>
              <div>
                <span>{data.namePublishedIn}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.namePublishedInYear && data.namePublishedInYear !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Nombre publicado en el año</span>
              </div>
              <div>
                <span>{data.namePublishedInYear}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.higherClassification && data.higherClassification !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Clasificación superiorkingdom</span>
              </div>
              <div>
                <span>{data.higherClassification}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.kingdom && data.kingdom !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Reino</span>
              </div>
              <div>
                <span>{data.kingdom}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.phylum && data.phylum !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Filo</span>
              </div>
              <div>
                <span>{data.phylum}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.class && data.class !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Clase</span>
              </div>
              <div>
                <span>{data.class}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.order && data.order !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Orden</span>
              </div>
              <div>
                <span>{data.order}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.family && data.family !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Familia</span>
              </div>
              <div>
                <span>{data.family}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.genus && data.genus !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Género</span>
              </div>
              <div>
                <span>{data.genus}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.subgenus && data.subgenus !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Subgénero</span>
              </div>
              <div>
                <span>{data.subgenus}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.specificEpithet && data.specificEpithet !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Epíteto específico</span>
              </div>
              <div>
                <span>{data.specificEpithet}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.infraspecificEpithet && data.infraspecificEpithet !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Epíteto infraespecífico</span>
              </div>
              <div>
                <span>{data.infraspecificEpithet}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.taxonRank && data.taxonRank !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Categoría del taxón</span>
              </div>
              <div>
                <span>{data.taxonRank}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.verbatimTaxonRank && data.verbatimTaxonRank !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Categoría original del taxón</span>
              </div>
              <div>
                <span>{data.verbatimTaxonRank}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.scientificNameAuthorship && data.scientificNameAuthorship !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Autoría del nombre científico</span>
              </div>
              <div>
                <span>{data.scientificNameAuthorship}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.vernacularName && data.vernacularName !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Nombre común</span>
              </div>
              <div>
                <span>{data.vernacularName}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.nomenclaturalCode && data.nomenclaturalCode !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Código nomenclatural</span>
              </div>
              <div>
                <span>{data.nomenclaturalCode}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.taxonomicStatus && data.taxonomicStatus !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Estado taxonómico</span>
              </div>
              <div>
                <span>{data.taxonomicStatus}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.nomenclaturalStatus && data.nomenclaturalStatus !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Estado nomenclatural</span>
              </div>
              <div>
                <span>{data.nomenclaturalStatus}</span>
              </div>
            </div>
          </div>
      }
      {
        (data.taxonRemarks && data.taxonRemarks !== '') && <div>
            <div className="uk-grid-small uk-child-width-1-2" data-uk-grid="">
              <div>
                <span className="uk-text-bold">Comentarios del taxón</span>
              </div>
              <div>
                <span>{data.taxonRemarks}</span>
              </div>
            </div>
          </div>
      }
    </div>);
  }
}

export default Taxon;
