import React, {Component} from 'react';

import CommonNames from './details/CommonNames';
import Distribution from './details/Distribution';
import ExemplaryType from './details/ExemplaryType';
import References from './details/References';
import RegistrationElement from './details/RegistrationElement';
import SpeciesProfile from './details/SpeciesProfile';
import Taxon from './details/Taxon';
import TaxonDescription from './details/TaxonDescription';


class Details extends Component {

  render() {
    return (<div>
      <div className="uk-flex uk-flex-column">
        <h3 className="uk-margin-left uk-text-bold">{this.props.title}</h3>
        <div className="uk-background-primary" style={{
            height: 2
          }}></div>
      </div>
      {this.props.children}
    </div>);
  }
}

Details.CommonNames = CommonNames;
Details.Distribution = Distribution;
Details.ExemplaryType = ExemplaryType;
Details.References = References;
Details.RegistrationElement = RegistrationElement;
Details.SpeciesProfile = SpeciesProfile;
Details.Taxon = Taxon;
Details.TaxonDescription = TaxonDescription;

export default Details;
