import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import map from 'lodash/map'

import * as StatiticsService from '../../services/StatiticsService';

class GeneralStatistics extends Component {

  static defaultProps = {
    id: undefined,
    param: undefined
  }

  constructor() {
    super();
    this.state = {
      stats: null
    }
  }
  
  /*
  componentDidMount() {
    StatiticsService.getStats(this.props.id, this.props.param).then(data => {
      if(this.props.id===undefined && this.props.param===undefined){
        data.values[0]={name: "LISTAS", value: 5}
        data.values.splice(1, 0, {name: "DE REFERENCIA", value: 2})
        data.values[2]={name: "PUBLICADORES", value: 5}
        data.values.splice(3, 1)
      }*/

  componentDidMount() {
    StatiticsService.getStats(this.props.id, this.props.param).then(data => {
      let d = []
      //console.log("Estadisticas: ", this.props.id)
      data[1].name="LISTAS"
      data[0].name="TAXA"
      if (this.props.id===undefined){
        d[0] = data[1]
        d[1] = data[2]
      }else if (this.props.id==="gbifId"){
        d[0] = data[0]
        d[1] = data[4]
      }else if (this.props.id==="organizationId"){
        d[0] = data[0]
        d[1] = data[1]
      }
      this.setState({ stats: d })
    }).catch(err => {
      console.error(err)
    })
  }


  render() {
    const { stats } = this.state;
    return (
      stats && <div className="uk-card uk-card-default uk-padding-small uk-card-body" >
        <div className="uk-grid-divider uk-child-width-expand@s uk-text-center uk-grid-small" data-uk-grid>
          {
            stats && map(stats, (v, k) => (
              <div key={k} className="uk-grid-collapse uk-child-width-1" data-uk-grid>
                <div className="uk-text-small">{v.name}</div>
                <span className="uk-h2"><NumberFormat value={v.value} displayType="text" thousandSeparator="." decimalSeparator="," /></span>
              </div>
            ))
          }
        </div>
      </div>
    )
  }
}

export default GeneralStatistics;
