import React, { Component } from 'react';
import cx from 'classnames';

export class DataSheet extends Component {
  render() {
    return (
      <div id={this.props.scroll} className={cx('uk-card uk-card-default uk-padding', this.props.className)}>
        <div className="uk-flex uk-flex-column">
          <h3 className="uk-margin-left uk-text-bold">{this.props.title}</h3>
          <div className="uk-width-1-4 uk-width-small@s uk-background-primary" style={{ height: 2 }}></div>
        </div>
        {this.props.children}
      </div>
    )
  }
}

export default DataSheet;
