import React, { Component } from 'react';
import { findIndex, findKey, map, differenceWith, isEqual, lowerCase } from 'lodash';
import { Map, TileLayer, FeatureGroup } from 'react-leaflet';
import { EditControl } from "react-leaflet-draw"

import Filters from '../Filters';

class SearchMap extends Component {
  constructor() {
    super();
    this.state = {
      data: null,
    }
    this.filters = [];
    this.values = [
      { id: 'foo', label: 'Foo' },
      { id: 'bar', label: 'Bar' },
      { id: 'baz', label: 'Baz' },
    ];
    this.query = [];

  }

  componentWillMount() {
    this.props.onRef(this)
  }


  handleFilter(value) {
    const item = findIndex(this.query, value)
    if (item < 0) {
      this.query.push(value)
    }

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.query, value);
    this.query.splice(position, 1);

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  getValues(values) {
    let filter = [];
    map(this.query, (value) => {
      filter.unshift({ id: lowerCase(value[Object.keys(value)]), label: value[Object.keys(value)] })
    })
    return differenceWith(values, filter, isEqual)
  }

  cleanFilters() {
    this.query = [];
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  _onCreate(e) {
    // console.log("Mapa creado", e)
    // const type = e.layerType;
    const layer = e.layer;

    // When a user finishes editing a shape we get that information here
    // console.log('draw:created->');
    // console.log(JSON.stringify(layer.toGeoJSON()));
    let q = JSON.stringify(layer.toGeoJSON().geometry.coordinates);
    q = q.replace(/\[/g, '');
    q = q.replace(/,/g, ' ');
    q = q.replace(/\]/g, ',');
    q = q.replace(/,,,/g, '');
    q = "POLYGON ((" + q + "))";

    // console.log("La consulta es ", q)
    this.query.push({ id: 'geometry=' + q, label: q, idQ: '', labelQ: '', value: '' })
    // console.log("query antes de enviarlo ", this.query)
    this.props.count(this.query)
    this.setState({ data: this.query })



    /*
    
    geometry=POLYGON ((-75.498046875 8.207141884705385, -76.37695312499999 2.170380271968855, -71.455078125 -0.46581410806627527, -70.13671875 5.677211791581823, -73.916015625 4.276298816089856, -75.498046875 8.207141884705385))
    
    */


  }
  _onDeleted(x) {
    this.query = [];
    this.props.count(this.query)
    this.setState({ data: this.query })
  }
  _onEditPath(x) {
    // console.log("Mapa editado", x)
  }

  render() {
    // console.log("Construyendo mapa de busqueda")
    return (
      <Filters.Base expand={true} title="Mapa" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)}>
        <Map style={{ width: "100%", height: "300px" }} center={[4.36, -74.04]} zoom={4} zoomControl={false}>
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png"
          />
          <FeatureGroup>
            <EditControl
              position='topright'
              onEdited={this._onEditPath}
              onCreated={(e) => { this._onCreate(e) }}
              onDeleted={(e) => { this._onDeleted(e) }}
              draw={{
                circle: false,
                line: false,
                marker: false,
                circlemarker: false,
                polyline: false,
              }}
            />
          </FeatureGroup>
        </Map>
      </Filters.Base>
    );
  }
}

export default SearchMap;
