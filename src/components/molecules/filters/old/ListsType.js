import React, { Component } from 'react';
import Autocomplete from '../../atoms/Autocomplete';
import { map, differenceWith, isEqual, findKey, findIndex } from 'lodash';

import Filters from '../Filters';

class ListsType extends Component {

  constructor() {
    super();
    this.state = {
      data: null,
    }

    this.values = [];
    this.filters = [];
    this.query = [];
  }

  componentWillMount() {
    const sFilters = [
      { id: 'type=etapa1', label: 'Tipo 1', idQ: 'tipo1', labelQ: 'type', value: 30000 },
      { id: 'type=etapa2', label: 'Tipo 2', idQ: 'tipo2', labelQ: 'type', value: 20000 },
      { id: 'type=etapa3', label: 'Tipo 3', idQ: 'tipo3', labelQ: 'type', value: 10000 },
    ]

    map(sFilters, (value) => {
      this.values.unshift(value);
      this.filters.unshift(value);
    })

    this.props.onRef(this)
    this.activeFilters(this.props.activeFilters)
  }

  handleFilter(value, active) {
    const i = findIndex(this.values, (o) => { return o.label === value || o.label === value.label });
    const obj = this.values[i];
    const item = findIndex(this.query, obj)
    if (item < 0)
      this.query.unshift(obj)

    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.query, value);
    this.query.splice(position, 1);
    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  getValues(values) {
    let filter = [];
    map(this.query, (value) => {
      filter.unshift(value)
    })
    return differenceWith(values, filter, isEqual)
  }

  cleanFilters() {
    this.query = [];
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  activeFilters(data) {
    map(data, (item) => {
      const i = findKey(this.values, (o) => { return o.id === item })
      if (i >= 0) {
        this.handleFilter(this.values[i].label, true)
      }
    })
  }

  render() {
    return (
      <Filters.Base title="Tipo de lista" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)}>
        <Autocomplete values={this.getValues(this.values)} placeholder="Escriba el tipo" selectValue={(value) => this.handleFilter(value)} />
        <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
          {
            map((differenceWith(this.filters, this.query, isEqual)), (value, key) =>
              <label key={key} className="uk-form-small">
                <input className="uk-checkbox uk-margin-small-right" type="checkbox" onClick={(e) => { e.preventDefault(); this.handleFilter(value) }} />{value.label}
                <span className="uk-float-right">{value.value}</span>
              </label>
            )
          }
        </div>
      </Filters.Base>
    );
  }
}

export default ListsType;
