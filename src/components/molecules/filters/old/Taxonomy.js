import React, { Component } from 'react';
import { lowerCase, findIndex, findKey, map, differenceWith, isEqual } from 'lodash';

import Filters from '../Filters';
import Autocomplete from '../../atoms/Autocomplete';

class Taxonomy extends Component {

  constructor() {
    super();
    this.state = {
      select: 'Nombre científico',
      data: null,
    }

    this.handleSelect = this.handleSelect.bind(this);

    this.filters = {
      'Nombre científico': 'scientificName',
      'Reino': 'kingdom',
      'Filo': 'phylum',
      'Clase': 'class',
      'Orden': 'order',
      'Familia': 'family',
      'Género': 'genus',
      'Epíteto específico': 'specificEpithet',
      'Epíteto infraespecífico': 'infraSpecificEpithet',
    }

    this.values = [
      { id: 'taxon_key=1', label: 'Animalia', idQ: '1', labelQ: 'kingdom', value: 'Animalia' },
      { id: 'taxon_key=6', label: 'Plantae', idQ: '6', labelQ: 'kingdom', value: 'Plantae' },
      { id: 'taxon_key=8', label: 'Virus', idQ: '8', labelQ: 'kingdom', value: 'Virus' },
    ];

    this.query = [];
    this.child = null;
  }

  handleSelect(e) {
    this.setState({
      select: e.target.value,
    })
  }

  componentWillMount() {
    this.props.onRef(this)
    this.activeFilters(this.props.activeFilters)
  }

  handleFilter(value) {
    const i = findIndex(this.values, (o) => { return o.label === value });
    const obj = this.values[i];
    obj.label = 'Reino';
    const item = findIndex(this.query, obj)
    if (item < 0)
      this.query.unshift(obj)

    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    value.label = value.value
    const position = findKey(this.query, value);
    this.query.splice(position, 1);
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  getValues(values) {
    let filter = [];
    map(this.query, (value) => {
      filter.unshift(value)
    })
    return differenceWith(values, filter, isEqual)
  }

  cleanFilters() {
    this.query = []
    this.child.handleDrop(false)
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  activeFilters(data) {
    map(data, (item) => {
      const i = findKey(this.values, (o) => { return o.id === item })
      if (i >= 0) {
        this.handleFilter(this.values[i].label)
      }
    })
  }

  render() {

    return (
      <Filters.Base onRef={ref => { this.child = ref }} title="Taxonomía" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)}>
        <select className="uk-select uk-form-small" onChange={this.handleSelect}>
          <option>Nombre científico</option>
          <option>Reino</option>
          <option>Filo</option>
          <option>Clase</option>
          <option>Orden</option>
          <option>Familia</option>
          <option>Género</option>
          <option>Epíteto específico</option>
          <option>Epíteto infraespecífico</option>
        </select>
        <Autocomplete values={this.getValues(this.values)} placeholder={`Escriba el ${lowerCase(this.state.select)}`} selectValue={(value) => this.handleFilter(value)} />
      </Filters.Base>
    );
  }
}

export default Taxonomy;
