import React, { Component } from 'react';
import Autocomplete from '../../atoms/Autocomplete';
import { findIndex, findKey, map, differenceWith, isEqual } from 'lodash';

import Filters from '../Filters';

class TaxonID extends Component {

  constructor() {
    super();
    this.state = {
      data: null,
    }
    this.filters = [];
    this.values = [
      { id: 'id_catalog=catalog1', label: 'Número 1', idQ: 'catalog1', labelQ: 'id_catalog', value: 'Número 1' },
      { id: 'id_catalog=catalog2', label: 'Número 2', idQ: 'catalog2', labelQ: 'id_catalog', value: 'Número 2' },
      { id: 'id_catalog=catalog3', label: 'Número 3', idQ: 'catalog3', labelQ: 'id_catalog', value: 'Número 3' },
    ];
    this.query = [];
  }

  componentWillMount() {
    this.props.onRef(this)
    this.activeFilters(this.props.activeFilters);
  }


  handleFilter(value) {
    const i = findIndex(this.values, (o) => { return o.label === value });
    const obj = this.values[i];
    obj.label = 'Número';
    const item = findIndex(this.query, obj)
    if (item < 0)
      this.query.unshift(obj)

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    value.label = value.value
    const position = findKey(this.query, value);
    this.query.splice(position, 1);

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  getValues(values) {
    let filter = [];
    map(this.query, (value) => {
      filter.unshift(value)
    })
    return differenceWith(values, filter, isEqual)
  }

  cleanFilters() {
    this.query = []
    this.props.count(this.filters)
    this.setState({ data: this.query })
  }

  activeFilters(data) {
    map(data, (item) => {
      const i = findKey(this.values, (o) => { return o.id === item })
      if (i >= 0) {
        this.handleFilter(this.values[i].label)
      }
    })
  }

  render() {
    return (
      <Filters.Base title="ID del taxón" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)}>
        <Autocomplete values={this.getValues(this.values)} placeholder="Escribe el número" selectValue={(value) => this.handleFilter(value)} />
      </Filters.Base>
    );
  }
}

export default TaxonID;
