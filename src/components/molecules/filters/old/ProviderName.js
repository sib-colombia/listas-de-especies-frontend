import React, { Component } from 'react';
import Autocomplete from '../../atoms/Autocomplete';
import { map, differenceWith, isEqual, findKey, findIndex, forEach } from 'lodash';
import NumberFormat from 'react-number-format';

import Filters from '../Filters';

class ProviderName extends Component {

  constructor() {
    super();
    this.state = {
      data: null,
    }

    this.percent = [];
    this.values = [];
    this.filters = [];
    this.query = [];
  }

  componentWillMount() {
    if (this.props.filters && this.props.filters.PUBLISHING_ORG && this.props.filters.PUBLISHING_ORG.counts) {
      forEach(this.props.filters.PUBLISHING_ORG.counts, (value, key) => {
        this.filters.push({ id: 'publishing_org=' + key, label: value.title, idQ: key, labelQ: 'publishing_org', value: value.count, fraction: parseInt(value.fraction * 100, 10) })
        this.values.push({ id: 'publishing_org=' + key, label: value.title, idQ: key, labelQ: 'publishing_org', value: value.count, fraction: parseInt(value.fraction * 100, 10) })
      })
    }

    this.props.onRef(this)
    this.activeFilters(this.props.activeFilters);
  }


  handleFilter(value) {
    const i = findIndex(this.values, (o) => { return o.label === value || o.label === value.label });
    const obj = this.values[i];
    const item = findIndex(this.query, obj)
    if (item < 0)
      this.query.unshift(obj)

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.query, value);
    this.query.splice(position, 1);

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  getValues(values) {
    let filter = [];
    map(this.query, (value) => {
      filter.unshift(value)
    })
    return differenceWith(values, filter, isEqual)
  }

  cleanFilters() {
    this.query = [];
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  activeFilters(data) {
    map(data, (item) => {
      const i = findKey(this.values, (o) => { return o.id === item })
      if (i >= 0) {
        this.handleFilter(this.values[i].label)
      }
    })
  }

  render() {
    return (
      <Filters.Base title="Publicador" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)}>
        <Autocomplete values={this.getValues(this.values)} placeholder={`Escribe el nombre del publicador`} selectValue={(value) => this.handleFilter(value)} />
        <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
          {
            map((differenceWith(this.filters, this.query, isEqual)), (value, key) =>
              <label key={key} className="uk-form-small" style={{
                background: "url(/images/fondo.png) no-repeat",
                backgroundSize: value.fraction + "% 100%"
              }}>
                <span>
                  <input className="uk-checkbox uk-margin-small-right" type="checkbox" onClick={(e) => { e.preventDefault(); this.handleFilter(value) }} />
                  {(value.label).substring(0, 30)}
                  {(value.label).length > 30 && "..."}
                </span>

                <span className="uk-float-right">
                  <NumberFormat value={value.value} displayType="text" thousandSeparator />
                </span>
              </label>
            )
          }
        </div>
      </Filters.Base>
    );
  }
}

export default ProviderName;
