import React, { Component } from 'react';
import Autocomplete from '../../atoms/Autocomplete';
import { findIndex, findKey, map, differenceWith, isEqual } from 'lodash';

import Filters from '../Filters';

class Doi extends Component {

  constructor() {
    super();
    this.state = {
      data: null,
    }
    this.filters = [];
    this.values = [
      { id: 'doi=1', label: 'doi://1', idQ: '1', labelQ: 'doi', value: 'doi://1' },
      { id: 'doi=2', label: 'doi://2', idQ: '2', labelQ: 'doi', value: 'doi://2' },
      { id: 'doi=3', label: 'doi://3', idQ: '3', labelQ: 'doi', value: 'doi://3' },
    ];
    this.query = [];
  }

  componentWillMount() {
    this.props.onRef(this)
    this.activeFilters(this.props.activeFilters);
  }


  handleFilter(value) {
    const i = findIndex(this.values, (o) => { return o.label === value });
    const obj = this.values[i];
    obj.label = 'DOI';
    const item = findIndex(this.query, obj)
    if (item < 0)
      this.query.unshift(obj)

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    value.label = value.value
    const position = findKey(this.query, value);
    this.query.splice(position, 1);

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  getValues(values) {
    let filter = [];
    map(this.query, (value) => {
      filter.unshift(value)
    })
    return differenceWith(values, filter, isEqual)
  }

  cleanFilters() {
    this.query = []
    this.props.count(this.filters)
    this.setState({ data: this.query })
  }

  activeFilters(data) {
    map(data, (item) => {
      const i = findKey(this.values, (o) => { return o.id === item })
      if (i >= 0) {
        this.handleFilter(this.values[i].label)
      }
    })
  }

  render() {
    return (
      <Filters.Base title="DOI" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)}>
        <Autocomplete values={this.getValues(this.values)} placeholder="Escribe el DOI" selectValue={(value) => this.handleFilter(value)} />
      </Filters.Base>
    );
  }
}

export default Doi;
