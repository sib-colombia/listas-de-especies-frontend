import React, { Component } from 'react';
import Autocomplete from '../../atoms/Autocomplete';
import { map, differenceWith, isEqual, findKey, findIndex, lowerCase } from 'lodash';

import Filters from '../Filters';

class Location extends Component {

  constructor() {
    super();
    this.state = {
      select: 'País',
      data: null,
    }

    this.values = [];
    this.filters = [];
    this.query = [];
    this.handleSelect = this.handleSelect.bind(this);
  }

  componentWillMount() {
    const sFilters = [
      { id: 'state_province=amazonas', label: 'Amazonas', idQ: 'amazonas', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=antioquia', label: 'Antioquia', idQ: 'antioquia', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=arauca', label: 'Arauca', idQ: 'arauca', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=atlántico', label: 'Atlántico', idQ: 'atlántico', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=bogotá', label: 'Bogotá', idQ: 'bogotá', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=bolívar ', label: 'Bolívar ', idQ: 'bolívar ', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=boyacá', label: 'Boyacá', idQ: 'boyacá', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=caldas', label: 'Caldas', idQ: 'caldas', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=caquetá', label: 'Caquetá', idQ: 'caquetá', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=casanare', label: 'Casanare', idQ: 'casanare', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=cauca', label: 'Cauca', idQ: 'cauca', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=cesar', label: 'Cesar', idQ: 'cesar', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=chocó', label: 'Chocó', idQ: 'chocó', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=córdoba', label: 'Córdoba', idQ: 'córdoba', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=cundinamarca', label: 'Cundinamarca', idQ: 'cundinamarca', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=guainía', label: 'Guainía', idQ: 'guainía', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=guaviare', label: 'Guaviare', idQ: 'guaviare', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=huila', label: 'Huila', idQ: 'huila', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=la guajira	', label: 'La Guajira	', idQ: 'la guajira	', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=magdalena', label: 'Magdalena', idQ: 'magdalena', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=meta', label: 'Meta', idQ: 'meta', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=nariño', label: 'Nariño', idQ: 'nariño', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=norte de santander', label: 'Norte de Santander', idQ: 'norte de santander', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=putumayo', label: 'Putumayo', idQ: 'putumayo', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=quindío', label: 'Quindío', idQ: 'quindío', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=risaralda', label: 'Risaralda', idQ: 'risaralda', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=san andrés y providencia	', label: 'San Andrés y Providencia	', idQ: 'san andrés y providencia	', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=santander', label: 'Santander', idQ: 'santander', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=sucre', label: 'Sucre', idQ: 'sucre', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=tolima', label: 'Tolima', idQ: 'tolima', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=valle del cauca', label: 'Valle del Cauca', idQ: 'valle del cauca', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=vaupés', label: 'Vaupés', idQ: 'vaupés', labelQ: 'state_province', value: 30000 },
      { id: 'state_province=vichada', label: 'Vichada', idQ: 'vichada', labelQ: 'state_province', value: 30000 },
    ]

    map(sFilters, (value) => {
      this.values.push(value);
      this.filters.push(value);
    })

    this.props.onRef(this)
    this.activeFilters(this.props.activeFilters)
  }

  handleSelect(e) {
    this.setState({
      select: e.target.value,
    })
  }

  handleFilter(value, active) {
    const i = findIndex(this.values, (o) => { return o.label === value || o.label === value.label });
    const obj = this.values[i];
    const item = findIndex(this.query, obj)
    if (item < 0)
      this.query.push(obj)

    // TODO: Quitar linea
    !active && window.location.reload();
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.query, value);
    this.query.splice(position, 1);
    // TODO: Quitar linea
    window.location.reload();
    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  getValues(values) {
    let filter = [];
    map(this.query, (value) => {
      filter.unshift(value)
    })
    return differenceWith(values, filter, isEqual)
  }

  cleanFilters() {
    this.query = [];
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  activeFilters(data) {
    map(data, (item) => {
      const i = findKey(this.values, (o) => { return o.id === decodeURIComponent(item) })
      if (i >= 0) {
        this.handleFilter(this.values[i].label, true)
      }
    })
  }

  render() {
    return (
      <Filters.Base title="Ubicación" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)}>
        <select className="uk-select uk-form-small" onChange={this.handleSelect}>
          <option>País</option>
          <option>Departamento</option>
          <option>Municipio</option>
          <option>Localidad</option>
        </select>
        <Autocomplete values={this.getValues(this.values)} placeholder={`Escriba el ${lowerCase(this.state.select)}`} selectValue={(value) => this.handleFilter(value)} />
        <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
          {
            map((differenceWith(this.filters, this.query, isEqual)), (value, key) =>
              <label key={key} className="uk-form-small">
                <input className="uk-checkbox uk-margin-small-right" type="checkbox" onClick={(e) => { e.preventDefault(); this.handleFilter(value) }} />{value.label}
                <span className="uk-float-right">{value.value}</span>
              </label>
            )
          }
        </div>
      </Filters.Base>
    );
  }
}

export default Location;
