import React, { Component } from 'react';
import { map, differenceWith, isEqual, findKey, forEach } from 'lodash';
import NumberFormat from 'react-number-format';

import Filters from '../Filters';

class License extends Component {

  constructor() {
    super();
    this.state = {
      data: null
    };

    this.percent = [];
    this.filters = [];
    this.query = [];
  }

  componentWillMount() {
    if (this.props.filters && this.props.filters.LICENSE && this.props.filters.LICENSE.counts) {
      forEach(this.props.filters.LICENSE.counts, (value, key) => {
        this.filters.push({ id: 'license=' + key, label: value.title, idQ: key, labelQ: 'license', value: value.count, fraction: parseInt(value.fraction * 100, 10) })
      })
    }

    this.props.onRef(this)
    this.activeFilters(this.props.activeFilters);
  }

  handleFilter(value) {
    this.query.unshift(value)
    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.query, value);
    this.query.splice(position, 1)

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  cleanFilters() {
    this.query = [];
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  activeFilters(data) {
    map(data, (item) => {
      const i = findKey(this.filters, (o) => { return o.id === item })
      if (i >= 0) {
        this.handleFilter(this.filters[i])
      }
    })
  }

  render() {
    return (
      <Filters.Base title="Licencia" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)}>
        <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
          {
            map((differenceWith(this.filters, this.query, isEqual)), (value, key) =>
              <label key={key} className="uk-form-small" style={{
                background: "url(/images/fondo.png) no-repeat",
                backgroundSize: value.fraction + "% 100%"
              }}>
                <input className="uk-checkbox uk-margin-small-right" type="checkbox" onClick={(e) => { e.preventDefault(); this.handleFilter(value) }} />{value.label}
                <span className="uk-float-right">
                  <NumberFormat value={value.value} displayType="text" thousandSeparator />
                </span>
              </label>
            )
          }
        </div>
      </Filters.Base>
    );
  }
}

export default License;
