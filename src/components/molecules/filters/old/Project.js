import React, { Component } from 'react';

import Filters from '../Filters';

class Project extends Component {
  constructor() {
    super();
    this.state = {
      data: [{ id: 'project=ColombiaBIO', label: 'Colombia BIO', idQ: 'ColombiaBIO', labelQ: 'project', value: 8500 }]
    }

  }

  componentWillMount() {
    this.props.onRef(this);
    this.props.count(this.state.data);
  }

  cleanFilters() {
    this.props.count(this.state.data);
  }

  render() {
    return (
      <Filters.Base title="Proyecto" handlerFilter={this.state.data}>
        {/* <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
          <label className="uk-form-small">
            <input className="uk-checkbox uk-margin-small-right" defaultChecked type="checkbox" />Colombia BIO<span className="uk-float-right">8.523</span>
          </label>
        </div> */}
      </Filters.Base>
    );
  }
}

export default Project;