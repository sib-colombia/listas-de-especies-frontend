import React, {Component} from 'react';
import {Rectangle} from 'react-leaflet';

import map from 'lodash/map';

class GeographicCoverages extends Component {

  render() {
    return (
      <div>
        {
          this.props.g.boundingCoordinates &&
          <Rectangle bounds={[
            [this.props.g.boundingCoordinates.southBoundingCoordinate, this.props.g.boundingCoordinates.eastBoundingCoordinate],
            [this.props.g.boundingCoordinates.northBoundingCoordinate, this.props.g.boundingCoordinates.westBoundingCoordinate]
          ]}/>
        }
        { map(this.props.g,(r, key)  => {
          if (r.boundingBox)
            return <Rectangle key={key} bounds={[
              [r.boundingBox.minLatitude, r.boundingBox.minLongitude],
              [r.boundingBox.maxLatitude, r.boundingBox.maxLongitude]
            ]}/>
        })}
      </div>
    );
  }
}

export default GeographicCoverages;
