import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { URL_CATALOGO, URL_COLECCIONES, URL_PORTAL } from '../../config/const'


class SpeciesRow extends Component {
  render() {
    let { data } = this.props;
    data = data._source
    return (
      <tr>
      {/*
        <td className="uk-flex uk-flex-around">
          <Link to={URL_PORTAL+"search?scientificName="+data.genus+" "+data.specificEpithet} target="_blank" ><span className="uk-badge uk-background-quaternary" style={{ width: 12, height: 12, minWidth: 0 }} /></Link>
          <Link to={URL_COLECCIONES+"search?scientificName="+data.genus+" "+data.specificEpithet} target="_blank" ><span className="uk-badge uk-background-tertiary" style={{ width: 12, height: 12, minWidth: 0 }} /></Link>
          <Link to={URL_CATALOGO+"search/basic?scientificName="+data.genus+" "+data.specificEpithet} target="_blank" ><span className="uk-badge" style={{ width: 12, height: 12, minWidth: 0 }} /></Link>
        </td>*/}
        <td>{data.scientificName}</td>
        <td>{data.kingdom}</td>
        <td>{data.phylum}</td>
        <td>{data.class}</td>
        <td>{data.order}</td>
        <td>{data.family}</td>
        <td>{data.genus}</td>
        <td>{data.specificEpithet}</td>
        <td>{data.taxonRank}</td>
        <td>{data.vernacularName}</td>
        <td>{data.threatStatus}</td>
        <td>{data.appendixCITES}</td>
      </tr>
    );
  }
}

export default SpeciesRow;
