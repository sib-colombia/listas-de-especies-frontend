import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import NumberFormat from 'react-number-format';

import { getKeyword } from '../../util';

class ListsGridItem extends Component {


  render() {
    const { data } = this.props;
    let x = data.platform.hits.hits[0]._source
    return (
      <div>
        <div className="uk-card uk-card-default uk-child-width-1-1 uk-grid-collapse" data-uk-grid>
          <div className="uk-flex uk-flex-center" >
            <Link className="uk-link-reset uk-inline uk-width-1" to={`/list/${x.organizationId}`}>
              <img alt="" className="uk-width-1" src={x.resourceLogoUrl} />
            </Link>
          </div>
          <h5 className="uk-text-tertiary uk-text-truncate uk-text-bold uk-margin-small-left uk-margin-small-right uk-margin-small-top">
            <Link className="uk-link-reset" to={`/list/${x.organizationId}`}>{x.titleResource}</Link>
          </h5>
          <h6 className="uk-margin-small-left uk-margin-small-bottom">
            <Link className="uk-link-reset" to={`/list/${x.organizationId}`}>{x.organizationName}</Link>
          </h6>
          <div className={`${getKeyword(x.keywordCollections, "Taxonomic Authority") ? 'uk-background-primary' : 'uk-background-tertiary'} uk-text-default uk-text-small uk-flex uk-flex-middle uk-maximo-20`} >
            {getKeyword(x.organizationIdwordCollections, "Taxonomic Authority") && <div style={{ borderRight: 'solid 1px #fff', padding: 3 }} ><span uk-icon="icon: star"></span></div>}
            <Link className="uk-link-reset" to={`/list/${x.organizationId}`}><span className="uk-text-bold uk-margin-small-left"><NumberFormat value={data.doc_count} displayType="text" thousandSeparator /></span> Taxa</Link>
          </div>
        </div>
      </div>
    );
  }
}

export default ListsGridItem;
