import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';

import Home from './pages/Home';
import Specie from './pages/details/Specie';
import Providers from './pages/Providers';
import Provider from './pages/details/Provider';
import Lists from './pages/Lists';
import List from './pages/details/List';
import NotFound from './pages/NotFound';
import Static from './pages/Static';

UIkit.use(Icons);

class App extends Component {

  render() {
    return (
      <Switch>
        <Route exact path="/" component={Home} />

        <Route path="/specie/:id" component={Specie} />

        <Route path="/providers" component={Providers} />
        <Route path="/provider/:id" component={Provider} />

        <Route path="/lists" component={Lists} />
        <Route path="/list/:id" component={List} />

        <Route path="/static/:id" component={Static} />

        <Route component={NotFound} />
      </Switch>
    );
  }
}

export default App;
