import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import cx from 'classnames';

class GenericPage extends Component {

  constructor() {
    super();
    this.state = {
      menu: true,
    }
  }

  render() {
    return (
      <div className={cx('uk-scope', this.props.className)}>
        <Helmet>
          <title>{this.props.titlep}</title>
        </Helmet>
        <this.props.header.type menu={() => this.setState({ menu: !this.state.menu })}
          openAdvanceSearch={() => { this.setState({ menu: false }) }} />
        <div className="uk-background-muted" data-uk-height-viewport="expand:true;">
          {this.props.children}
        </div>
        {this.props.footer}
      </div>
    );
  }
}

export default GenericPage;
