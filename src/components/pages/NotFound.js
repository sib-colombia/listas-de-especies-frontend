import React, { Component } from 'react';

import NotFoundBoo from '../atoms/NotFoundBoo';
import GenericPage from '../templates/GenericPage';
import Header from '../organisms/Header';
import Footer from '../organisms/Footer';

export class NotFound extends Component {
  render() {
    return (
    <GenericPage titlep="Listas de Especies" header={<Header />} footer={<Footer />}>
      <NotFoundBoo />
    </GenericPage>
    );
  }
}

export default NotFound;
