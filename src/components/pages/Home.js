import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import GenericPage from '../templates/GenericPage';
import Header from '../organisms/Header';
import Footer from '../organisms/Footer';
import GeneralStatistics from '../molecules/GeneralStatistics';

class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      height: NaN,
      data: null,
    }

    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {
    this.updateDimensions();
    this.setState({
      data: {
        'LISTAS': 4600,
        'DE REFERENCIA': 4,
        'PUBLICADOR': 4600
      }
    })
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions)
  }

  updateDimensions() {
    this.setState({
      height: (window.innerHeight / 2)
    })
  }

  render() {
    return (<GenericPage titlep="Listas de Especies" header={<Header />} footer={<Footer />}>
      <div className="uk-position-relative uk-light" data-uk-slideshow={`min-height: ${this.state.height}; max-height: ${this.state.height}`}>
        <ul className="uk-slideshow-items">
          <li>
            <img src="https://statics.sibcolombia.net/sib-resources/images/listas+de+especies/jpg/murcielago-opt.jpg" alt="" data-uk-cover="data-uk-cover" />
            <div className="uk-overlay uk-overlay-primary uk-position-top-right uk-position-small uk-padding-small uk-text-center uk-light uk-visible@s">
              <p className="uk-text-small"><a target="_blank" rel="noopener noreferrer" href="https://www.flickr.com/photos/154330806@N06/36177125034">Mamíferos de Colombia - Licencia: CC BY-NC 2.0</a></p>
            </div>
          </li>
        </ul>
        <button className="uk-position-center-left uk-position-small uk-visible@s" data-uk-slidenav-previous="ratio: 1.5" uk-slideshow-item="previous" />
        <button className="uk-position-center-right uk-position-small uk-visible@s" data-uk-slidenav-next="ratio: 1.5" uk-slideshow-item="next" />
        <div className="uk-position-center uk-position-medium">
          <div className="uk-flex uk-flex-column">
            <Link to="/lists" style={{
              textDecoration: 'none'
            }}>
              <div className="uk-overlay uk-padding-small uk-text-center" style={{
                border: 'solid 1.5px #fff',
                cursor: 'pointer',
                backgroundColor: (
                  !this.state.hover
                    ? 'rgba(0,0,0,0.2)'
                    : 'rgba(255,255,255,0.2)'),
                transition: 'background-color 0.3s ease'
              }} onMouseEnter={() => this.setState({ hover: true })} onMouseLeave={() => this.setState({ hover: false })}>
                <h3 className="uk-text-bold">EXPLORA LAS LISTAS</h3>
              </div>
            </Link>
          </div>
        </div>
        <div className="uk-position-bottom-center uk-position-medium">
          <ul className="uk-dotnav uk-flex-nowrap uk-margin-bottom">
            <li uk-slideshow-item="0">
              <a>Item 1</a>
            </li>
            <li uk-slideshow-item="1">
              <a>Item 1</a>
            </li>
          </ul>
        </div>
      </div>
      <div className="uk-flex uk-flex-center uk-margin-left uk-margin-right" style={{
        marginTop: -35
      }}>
        <div className="uk-container uk-width-5-6@m uk-width-2-3@l"><GeneralStatistics /></div>
      </div>
      <div className="uk-section">
        <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
          <div className="uk-flex uk-flex-column">
            <h2 className="uk-margin-left">
              Explora a través de grupos biológicos
            </h2>
            <div className="uk-width-1-4 uk-width-small@s uk-background-primary" style={{
              height: 2
            }}></div>
          </div>
          <div className="uk-flex uk-flex-center uk-margin-top uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-auto@m uk-text-center" data-uk-grid="data-uk-grid">
            <Link className="uk-link-reset" to="/lists?phylum=Acanthocephala&phylum=Annelida&phylum=Arthropoda&phylum=Brachiopoda&phylum=Bryozoa&phylum=Chaetognatha&phylum=Cnidaria&phylum=Echinodermata&phylum=Gastrotricha&phylum=Mollusca&phylum=Myxozoa&phylum=Nematoda&phylum=Onychophora&phylum=Platyhelminthes&phylum=Porifera&phylum=Rotifera&phylum=Sipuncula">
              <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-invertebrados.png" alt=""/>
              <p className="uk-text-center">INVERTEBRADOS</p>
            </Link>
            <Link className="uk-link-reset" to="/lists?phylum=Chordata">
              <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-vertebrados.png" alt=""/>
              <p className="uk-text-center">VERTEBRADOS</p>
            </Link>
            <Link className="uk-link-reset" to="/lists?kingdom=Plantae">
              <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-plantas.png" alt=""/>
              <p className="uk-text-center">PLANTAS</p>
            </Link>
            <Link className="uk-link-reset" to="/lists?kingdom=Fungi">
              <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-hongos.png" alt=""/>
              <p className="uk-text-center">HONGOS</p>
            </Link>
            <Link className="uk-link-reset" to="/lists?kingdom=chromista">
              <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-chromista.png" alt=""/>
              <p className="uk-text-center">CHROMISTA</p>
            </Link>
            <Link className="uk-link-reset" to="/lists?kingdom=Archaea&kingdom=Bacteria&kingdom=incertae%20sedis&kingdom=Protozoa&kingdom=Viruses">
              <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-otros.png" alt=""/>
              <p className="uk-text-center">OTROS</p>
            </Link>
          </div>
        </div>
      </div>
      <div className="uk-section uk-section-small uk-section-default">
        <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
          <div className="uk-flex uk-flex-column uk-margin">
            <h2 className="uk-margin-left">
              Listas de referencia
            </h2>
            <div className="uk-width-1-4 uk-width-small@s uk-background-primary" style={{ height: 2 }} />
          </div>
          <div className="uk-position-relative" data-uk-slideshow="max-height: 370">
            <ul className="uk-slideshow-items">
              <li>
                <div className="uk-position-cover uk-overlay">
                  <div className="uk-flex uk-flex-middle" style={{ fontWeight: 200 }}>
                    <img className="uk-width-1-3 uk-float-left uk-margin-medium-right" src="https://statics.sibcolombia.net/sib-resources/images/logos-socios/portal-sib/logo-scmas.png" alt="" />
                    <div>
                      <h3>Lista de Mamíferos de Colombia</h3>
                      <p>Revisión de la riqueza de mamíferos de Colombia. La lista cuenta con 528 especies respaldadas por especímenes en colecciones biológicas, de las cuales 58 son endémicas del territorio nacional, 70 se encuentran en algún grado de amenaza y 89 son objeto de comercio.</p>
                      <Link to="/list/e8b9ed9b-f715-4eac-ae24-772fbf40d7ae"><button className="uk-button uk-button-secondary uk-text-bold uk-float-right">EXPLORA LA LISTA</button></Link>
                    </div>
                  </div>
                </div>
              </li>
              <li>
                <div className="uk-position-cover uk-overlay">
                  <div className="uk-flex uk-flex-middle" style={{ fontWeight: 200 }}>
                    <img className="uk-width-1-3 uk-float-left uk-margin-medium-right" src="https://statics.sibcolombia.net/sib-resources/images/logos-socios/portal-sib/logo-acictios.png" alt="" />
                    <div>
                      <h3>Lista de peces de agua dulce de Colombia</h3>
                      <p>Depuración y actualización del listado de peces de agua dulce del país, que arrojó 1494 especies, de las cuales 374 son endémicas de las cuencas hidrográficas de Colombia. Esta lista resulta fundamental en el proceso de conservación de las especies y ecosistemas acuáticos colombianos.</p>
                      <Link to="/list/7e3a2242-46d6-4b90-b80c-42c5d27ed93b"><button className="uk-button uk-button-secondary uk-text-bold uk-float-right">EXPLORA LA LISTA</button></Link>
                    </div>
                  </div>
                </div>
              </li>
                <li>
                    <div className="uk-position-cover uk-overlay">
                        <div className="uk-flex uk-flex-middle" style={{ fontWeight: 200 }}>
                            <img className="uk-width-1-3 uk-float-left uk-margin-medium-right" src="https://statics.sibcolombia.net/sib-resources/images/logos-socios/portal-sib/logo-minambiente.png" alt="" />
                            <div>
                                <h3>Lista de especies silvestres amenazadas Colombia</h3>
                                <p>A partir de la publicación oficial de la resolución 1912 de 2017, en la lista de especies amenazadas de Colombia, el Ministerio de Ambiente y Desarrollo Sostenible de Colombia a través de la evaluación y criterios de un comité técnico de expertos en el país, compila la información de las especies silvestres que se encuentra en alguna de las tres categorías de amenaza establecidas por la Unión Internacional para la Conservación de la Naturaleza - IUCN.</p>
                                <Link to="/list/772de164-541d-4db6-ba38-41ac1c8612c0"><button className="uk-button uk-button-secondary uk-text-bold uk-float-right">EXPLORA LA LISTA</button></Link>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div className="uk-position-cover uk-overlay">
                        <div className="uk-flex uk-flex-middle" style={{ fontWeight: 200 }}>
                            <img className="uk-width-1-3 uk-float-left uk-margin-medium-right" src="https://statics.sibcolombia.net/sib-resources/images/logos-socios/portal-sib/logo-aco.png" alt="" />
                            <div>
                                <h3>Lista de referencia de especies de aves de Colombia</h3>
                                <p>Esta lista recibió un detallado proceso de revisión por pares académicos, lo cual permite que la ACO la presente como lista de referencia para el país. En esta lista de referencia presentamos 1909 especies de aves presentes en el territorio continental e insular de Colombia, 216 más de las presentadas en la guía de Hilty y Brown (1986) "A Guide to the Birds of Colombia". Los registros adicionales compilan una exhaustiva revisión de 340 referencias relacionadas con la distribución de la avifauna colombiana, hasta agosto de 2017.</p>
                                <Link to="/list/6c9c4b08-4cec-4160-a708-7f16060d7db0"><button className="uk-button uk-button-secondary uk-text-bold uk-float-right">EXPLORA LA LISTA</button></Link>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <div className="uk-position-center uk-position-medium">
            </div>
            <div id="listas" className="uk-position-bottom-center">
              <ul className="uk-dotnav uk-flex-nowrap uk-margin-bottom">
                <li uk-slideshow-item="0"><a>Item 1</a></li>
                <li uk-slideshow-item="1"><a>Item 1</a></li>
              <li uk-slideshow-item="2"><a>Item 1</a></li>
              <li uk-slideshow-item="3"><a>Item 1</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div className="uk-section uk-light" style={{
        backgroundColor: '#00a8b4'
      }}>
        <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
          <div className="uk-flex uk-flex-center uk-flex-middle uk-child-width-1-1 uk-child-width-1-2@s" data-uk-grid="data-uk-grid">
            <div className="uk-text-center">
              <h1 className="uk-margin-remove">¿Quieres participar?</h1>
            </div>
            <div>
              <div className="uk-overlay uk-text-center uk-padding-small uk-link-reset" style={{
                border: 'solid 1.5px #fff'
              }}>
                <Link to="/static/sobre_el_portal">
                  <h2 className="uk-margin-remove">ENTÉRATE CÓMO</h2>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </GenericPage>);
  }
}

export default Home;
