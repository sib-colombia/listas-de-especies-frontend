import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';

import GenericPage from '../../templates/GenericPage';
import Header from '../../organisms/Header';
import Footer from '../../organisms/Footer';
import GeneralStatistics from '../../molecules/GeneralStatistics';
import * as ProviderService from '../../../services/ProviderService';
import Loading from '../../atoms/Loading';
import ContactsGrid from '../../organisms/ContactsGrid';
import * as ListService from '../../../services/ListService';
import ListsList from '../../organisms/ListsList';
import ListsGrid from '../../organisms/ListsGrid';
import Pagination from '../../atoms/Pagination';


class Provider extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: null,
      dataC: null,
      count: '',
      statitics: null,
    }
  }

  componentWillMount() {
    if (this.props.match.params.id !== undefined) {
      ProviderService.getProvider(this.props.match.params.id).then(data => {
        console.log("Llegaron los datos del publicador")
        console.log(data)
        this.setState({ data: data }, ()=>{
          this.offsetResults(0)
        })
      }).catch(err => {
        console.log(err)
      })
    }
    this.setState({
      statitics: {
        'LISTAS': 546321,
        'ESPECIES': 1800,
      }
    })
  }

  componentDidMount() {
    this.offsetResults(0);
  }

  onChangePage(pageOfItems) {
    const page = pageOfItems * 20;
    this.offsetResults(page, pageOfItems);
  }

  offsetResults(offset, pageOfItems) {
    console.log("consultando offsetResults", this.state)
    ListService.getDatasetList(offset, 'organizationId='+this.props.match.params.id).then(data => {
        this.setState({
        dataC: data.aggregations.gbifId.buckets,
        countC: "",
        })
      })
      
  }

  handleDisplay(state) {
    this.setState({ displayDatasets: state })
  }

  render() {
    let { data } = this.state;
    if (data===undefined || data===null) return <div></div>
    
    
    data = data.provider
    console.log("Publicador")
    console.log(data)
    return (
      <GenericPage
        titlep="Publicador - Listas de Especies"
        header={<Header />}
        footer={<Footer />}
      >
        {(data && <div className="uk-section uk-padding-remove-bottom">
          <div className="uk-flex uk-flex-center uk-grid-collapse" data-uk-grid>
            <div>
              <h4 className="uk-margin-left uk-heading-divider uk-margin-remove" style={{ borderBottomColor: '#ff7847' }}>PUBLICADOR</h4>
            </div>
          </div>
          <div className="uk-flex uk-flex-center uk-grid-collapse" data-uk-grid>
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l uk-text-center uk-margin-top uk-padding-remove">
              <h2>{data.title}</h2>
            </div>
          </div>
          <div className="uk-margin-top">
            <GeneralStatistics id={"organizationId"} param={this.props.match.params.id} />
          </div>
          <div className="uk-flex uk-flex-center uk-margin-top uk-margin">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <div className="uk-child-width-1-1 uk-child-width-expand@s" data-uk-grid>
                {data.homepage&&
                <div className="uk-width-1-4">
                  <Link to=""><img className="uk-preserved-width" src={data.resourceLogoUrl} alt="" /></Link>
                  <p className="uk-text-bold te"><span className="uk-text-top uk-margin-small-right" uk-icon="icon: link"></span><a className="uk-text-tertiary" href={data.homepage[0]} target="_blank" rel="">{data.homepage[0]}</a></p>
                </div>
                }
                <div>
                  <p>{data.description}</p>
                  <p className="uk-text-bold"><span className="uk-margin-small-right">Herramienta de publicación</span> <Link className="uk-text-tertiary" to=" ">Enlance</Link></p>
                </div>
              </div>
            </div>
          </div>
          <div className="uk-section uk-section-small uk-section-default">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <div className="uk-flex uk-flex-column">
                <h3 className="uk-margin-left uk-text-bold">Contactos</h3>
                <div className="uk-width-1-4 uk-width-small@s uk-background-primary" style={{ height: 2 }}></div>
              </div>
              <ContactsGrid data={data.contacts} grid="1-3" />
            </div>
          </div>
          <div className="uk-section uk-section-small">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <div className="uk-flex uk-flex-column">
                <h3 className="uk-margin-left uk-text-bold">Listas de especies</h3>
                <div className="uk-width-1-4 uk-width-small@s uk-background-primary" style={{ height: 2 }}></div>
              </div>
            </div>
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <ul className="uk-margin-medium-top uk-flex-right" data-uk-tab="swiping: false;">
                <li className={cx({ 'uk-active': !this.state.displayDatasets })} onClick={() => this.handleDisplay(false)}><a title="Modo lista" data-uk-tooltip><span uk-icon="icon: list"></span></a></li>
                <li className={cx({ 'uk-active': this.state.displayDatasets })} onClick={() => this.handleDisplay(true)}><a title="Moto grilla" data-uk-tooltip><span uk-icon="icon: grid"></span></a></li>
                {/*<li>
                  <a className="uk-text-right" title="Ordenar" data-uk-tooltip>A/Z</a>
                  <div className="uk-padding-small" data-uk-dropdown="mode: click">
                    <ul className="uk-nav uk-dropdown-nav">
                      <li><a>Alfabético</a></li>
                      <li><a>Recientes</a></li>
                      <li><a>Número de registros</a></li>
                    </ul>
                  </div>
                </li>
                */}
              </ul>
              {!this.state.displayDatasets ? <ListsList data={this.state.dataC} /> : <ListsGrid data={this.state.dataC} />}
              {this.state.key && this.state.dataC && <Pagination items={this.state.count} onChangePage={(number) => {
                this.onChangePage(number - 1);
              }} />}
            </div>
          </div>
        </div>) || <div className="uk-flex uk-flex-center uk-flex-middle uk-padding" data-uk-height-viewport="offset-top: true"><Loading /></div>}
      </GenericPage>
    );
  }
}

export default Provider;
