import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import HumboldtMap from '../../molecules/HumboldtMap';
import GeographicCoverages from '../../molecules/GeographicCoverages';
import capitalize from 'lodash/capitalize';
import {filter, map} from 'lodash';
import { route, getKeyword } from '../../../util';


import GenericPage from '../../templates/GenericPage';
import Header from '../../organisms/Header';
import Footer from '../../organisms/Footer';
import Doi from '../../atoms/Doi';
import License from '../../atoms/License';
import GeneralStatistics from '../../molecules/GeneralStatistics';
import DataSheet from '../../molecules/DataSheet';
import * as ListService from '../../../services/ListService';
import Loading from '../../atoms/Loading';
import ContactsGrid from '../../organisms/ContactsGrid';
import Reference from '../../atoms/Reference';
import SpeciesTable from '../../organisms/SpeciesTable';
import * as DatasetService from '../../../services/DatasetService';

import {URL_PORTAL} from '../../../config/const'



class List extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null
    }
  }

  componentWillMount() {
    if (this.props.match.params.id !== undefined) {
     ListService.getDataset(this.props.match.params.id).then(data => {
        this.setState({ data: data })
      }).catch(err => {
        console.log(err)
      })
    }
  }

  taxonomyCoverage(data, ruta) {
  
    let d = route(data, ruta)
    
    //console.log("taxonomyCoverage")
    //console.log(ruta)
    //console.log(data)
    //console.log(d)
    
    
    if (d===""){
      return <div>.</div>
    }
    let keys = {};
    // eslint-disable-next-line
    //console.log("taxonomyCoverage final")
    //console.log(d)
    
    data = d
    
    //console.log(data)
    if (Array.isArray(data)){
      //console.log(data)

      if(data[0] && data[0].generalTaxonomicCoverage!==undefined){
        return <span>{data.map((value) => this.taxonomyCoverage([value], [0]))}</span>
      }else{

        
        let d = data.map((value) => {
        
          // eslint-disable-next-line
          (value.coverages).map((coverage, key) => {
            // eslint-disable-next-line
            keys[coverage.rank.verbatim] = Array();
          });
          // eslint-disable-next-line
          (value.coverages).map((coverage, key) => {
            keys[coverage.rank.verbatim].push(coverage.scientificName)
          });
          return 1;
        });
        //console.log(d)


        return (Object.keys(keys)).map((values, key) => (<div key={key} className="uk-padding-small">
          <h4 className="uk-text-bold uk-margin-small">{capitalize(values)}</h4>
          <div className="uk-column-1-3">
            {
              keys[values].map((value, key) => (<div key={key}>
                <a className="uk-text-tertiary" href={URL_PORTAL+`search?${values}=${value}`} target="_blank">{value}</a>
              </div>))
            }
          </div>
        </div>))
      }
    }else{
      //console.log("Es una estructura base")
      //console.log(data.generalTaxonomicCoverage)
      //console.log(data.taxonomicClassification)
      return (<div className="uk-padding-small">
          
          <p className="uk-margin-small">{data.generalTaxonomicCoverage}</p>
          <div className="uk-column-1-3">
            {
              Array.isArray(data.taxonomicClassification) &&
              map(data.taxonomicClassification, (value, key) => (<div key={key}>
                <a className="uk-text-tertiary" href={URL_PORTAL+`search?${value.taxonRankName}=${value.taxonRankValue}`} target="_blank">{value.taxonRankValue} {value.commonName}</a>
              </div>))
            }
            {
              !Array.isArray(data.taxonomicClassification) &&
              <a className="uk-text-tertiary" href={URL_PORTAL+`search?${data.taxonomicClassification.taxonRankName}=${data.taxonomicClassification.taxonRankValue}`} target="_blank">{data.taxonomicClassification.taxonRankValue} {data.taxonomicClassification.commonName}</a>
            }
          </div>
        </div>)
    }
    
  }
  temporalCoverage(data) {
    console.log("temporalCoverage")
    console.log(data)
    if (!data){
      return <div></div>
    }
    
    if(data.rangeOfDates!==undefined){
      data = data.rangeOfDates;
      const beginDate = data.beginDate.calendarDate;
      const endDate = data.endDate.calendarDate;

      return (
        <p>
          <span className="uk-text-bold uk-margin-small-left uk-margin-small-right">Inicio:</span>
          {beginDate}
          <span data-uk-icon="icon: arrow-right" />
          <span className="uk-text-bold uk-margin-small-right">Fin:</span>
          {endDate}
        </p>
      )
    }
    if(data.singleDateTime!==undefined){
      return (
        <p>
          <span className="uk-text-bold uk-margin-small-left uk-margin-small-right">Fecha simple:</span>
          {data.singleDateTime.calendarDate}
        </p>
      )
    }
  }
  descargar(url) {
    window.open(url);
  }

  handleIdentifiers(data) {
    return data.map((value, key) => {
      return (value.type === 'DOI' && <Doi key={key} label={value.identifier} />) || ((value.type === 'UUID' && <p key={key}>
        <Link to={`/dataset/${value.identifier}`} className="uk-text-tertiary">{`${window.location.origin}/dataset/${value.identifier}`}</Link><br />
        <a href={`https://www.gbif.org/dataset/${value.identifier}`} className="uk-text-tertiary" target="_blank">https://www.gbif.org/dataset/{value.identifier}</a>
      </p>)) || (value.type && <p key={key}>
        <a href={value.identifier} target="_blank" className="uk-text-tertiary">{value.identifier}</a>
      </p>)
    })
  }

 
  render() {
    const {data} = this.state;
    console.log(data)
    //Manejo de valores indefinidos para studyExtent
    if (data){
      if (data && data.eml_raw && data.eml_raw['eml:eml'] && data.eml_raw['eml:eml'] && data.eml_raw['eml:eml'].dataset && data.eml_raw['eml:eml'].dataset.methods && data.eml_raw['eml:eml'].dataset.methods.sampling && data.eml_raw['eml:eml'].dataset.methods.sampling.studyExtent) {
        var studyExtent = data.eml_raw['eml:eml'].dataset.methods.sampling.studyExtent.description.para
      }
      else {
        var studyExtent = "No registra datos"
      console.log(studyExtent)}

      if (data && data.eml_raw && data.eml_raw['eml:eml'] && data.eml_raw['eml:eml'] && data.eml_raw['eml:eml'].dataset && data.eml_raw['eml:eml'].dataset.methods && data.eml_raw['eml:eml'].dataset.methods.sampling && data.eml_raw['eml:eml'].dataset.methods.sampling.samplingDescription) {
        var samplingDescription = data.eml_raw['eml:eml'].dataset.methods.sampling.samplingDescription.para
      }
      else {
        var samplingDescription = "No registra datos"
        console.log(samplingDescription)}

      if (data && data.eml_raw && data.eml_raw['eml:eml'] && data.eml_raw['eml:eml'] && data.eml_raw['eml:eml'].dataset && data.eml_raw['eml:eml'].dataset.methods && data.eml_raw['eml:eml'].dataset.methods.qualityControl) {
        var qualityControl = data.eml_raw['eml:eml'].dataset.methods.qualityControl.description.para
      }
      else {
        var qualityControl = "No registra datos"
        console.log(qualityControl)}


    }




    if (data){
      data.geographicCoverages = route(data, ["eml", 'eml:eml', "dataset", "coverage", "geographicCoverage"])
      //console.log(data.geographicCoverages)
      
      //console.log("data.keywordCollections= ", getKeyword(data.eml['eml:eml'].dataset.keywordSet, "Taxonomic Authority"))
      //console.log(data.eml['eml:eml'].dataset.keywordSet)
    }

    
    return (<GenericPage titlep="Listas de Especies" header={<Header />} footer={<Footer />}>
      {
        (data && <div className="uk-section uk-padding-remove-bottom">
          <div className="uk-flex uk-flex-center uk-grid-collapse uk-margin-remove" data-uk-grid="">
            <div>
              <h4 className="uk-margin-left uk-heading-divider" style={{
                borderBottomColor: '#ff7847'
              }}>LISTA DE ESPECIES</h4>
            </div>
          </div>
          <div className="uk-flex uk-flex-center uk-grid-collapse" data-uk-grid="data-uk-grid">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l uk-text-center uk-margin-top uk-padding-remove">
              <h2>{data.eml.titleResource}</h2>
            </div>
            <div className="uk-width-1-1 uk-text-center uk-margin-small-top">
              <h5 className="uk-text-tertiary">Publicado por
                <Link className="uk-text-tertiary uk-margin-small-left" to={`/provider/${data.organizationId}`} style={{
                  textDecoration: 'underline'
                }}>{data.organization.provider.title}</Link>
              </h5>
            </div>
            {getKeyword(data.eml_raw['eml:eml'].dataset.keywordSet, "Taxonomic Authority") &&
            <div className="uk-margin-small-top uk-width-1-6">
              <Reference />
            </div>}
          </div>
          <div className="uk-section uk-margin-large-top uk-heading-divider uk-padding-remove" style={{
            borderBottomWidth: 1
          }}>
            <div className="uk-container uk-container-small uk-padding-remove ">
              <div className="uk-grid-collapse uk-child-width-expand uk-margin uk-flex uk-flex-bottom" data-uk-grid="data-uk-grid">
                <div>
                  <ul className="uk-margin-medium-top" data-uk-tab="swiping: false;" data-uk-switcher="connect: .uk-switcher-list">
                    <li>
                      <a>DETALLE</a>
                    </li>
                    <li>
                      <a>VER EN TABLA</a>
                    </li>
                  </ul>
                </div>
                <div className="uk-width-auto">
                    <ul className="uk-margin-medium-top" data-uk-tab="swiping: false;">
                      <li>
                        <a className="uk-text-right">
                          <span className="uk-text-top" data-uk-icon="icon: download"></span>
                            Descarga</a>
                        <div data-uk-dropdown="mode: hover">
                          {
                            data.eml_raw["eml:eml"]["dataset"]["alternateIdentifier"]
                              .map((value, key) => {
                                console.log("Value: ", value)
                                if (value.substring(0,4)==="http"){
                                  return (
                                    <ul className="uk-nav uk-dropdown-nav" key={key}>
                                      <li>
                                        <a className="uk-text-tertiary" href={value} target="_blank" onClick={() => { this.descargar(value.replace("resource", "archive")) }} rel="noopener noreferrer" >DwC-A</a>
                                      </li>
                                      <li>
                                        <a className="uk-text-tertiary" href={value.replace("resource", "rtf")} target="_blank" onClick={() => { this.descargar(value.replace("resource", "rtf")) }} rel="noopener noreferrer" >RTF</a>
                                      </li>
                                      {/*<li>
                                        <a className="uk-text-tertiary" href={DatasetService.getUrlData(value.replace("resource", "archive"))} target="_blank" onClick={() => { this.descargar(DatasetService.getUrlData(value.replace("resource", "archive"))) }} rel="noopener noreferrer" >CSV</a>
                                      </li>*/}
                                    </ul>
                                  )
                                }
                                return <div></div>
                              })
                          }
                        </div>
                      </li>
                    </ul>
                  </div>
              </div>
            </div>
          </div>
          <ul className="uk-switcher uk-switcher-list">
            <div className="uk-section uk-section-xsmall">
              <div className="uk-flex uk-flex-center">
                <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
                  <div className="uk-child-width-1-1 uk-child-width-expand@s" data-uk-grid="data-uk-grid">
                    <div className="uk-width-auto">
                      <div style={{
                        width: 235,
                        height: 235
                      }} className="uk-flex uk-flex-center uk-flex-middle">
                        <Link to=""><img className="uk-preserved-width" width="150" height="150" src={data.eml.resourceLogoUrl} alt="" /></Link>
                      </div>
                      <div className="uk-margin-small-top"><Doi label={data.eml.doi} /></div>
                    </div>
                    <div>
                      <p>{data.eml.abstract.toString()}...
                        <a href="#descripcion" data-uk-scroll="offset: 95" className="uk-text-tertiary" to="">Ver más</a>
                      </p>
                      <p className="uk-margin-small">
                        <span className="uk-text-bold uk-margin-small-right">Ultima modificacíon:</span>
                        {data.modified && data.modified.substring(0,10)}</p>
                      <p className="uk-margin-small">
                        <span className="uk-text-bold uk-margin-small-right">Licencia:</span>
                        <License id={data.eml.licence_url} />
                      </p>
                      <p className="uk-margin-small">
                        <a className="uk-text-tertiary" href="#citacion" data-uk-scroll="offset: 90">¿Cómo citar?</a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <div className="uk-margin-top"><GeneralStatistics id="gbifId" param={data.gbifId} /></div>
              <div className="uk-section uk-section-xsmall">
                <div className="uk-flex uk-flex-center">
                  <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
                    <div className="uk-grid-small" data-uk-grid="data-uk-grid">
                      <div className="uk-width-1-4">
                        <div className="uk-card uk-card-default uk-padding-small" data-uk-sticky="offset: 90">
                          <ul className="uk-list uk-list-large uk-list-bullet uk-margin-remove-bottom" uk-scrollspy-nav="closest: li; scroll: true; cls: uk-text-primary">
                            <li>
                              <a className="uk-link-reset" href="#descripcion" data-uk-scroll="offset: 90">Descripción</a>
                            </li>
                            <li>
                              <a className="uk-link-reset" href="#cobertura_temporal" data-uk-scroll="offset: 90">Temporal</a>
                            </li>
                            { data.geographicCoverages &&
                            <li>
                              <a className="uk-link-reset" href="#cobertura_geografica" data-uk-scroll="offset: 90">Geográfica</a>
                            </li>
                            }
                            {route(data, ["eml", "eml:eml", "dataset", "coverage", "taxonomicCoverage"])!=="" &&
                              <li>
                                <a className="uk-link-reset" href="#cobertura_taxonomica" data-uk-scroll="offset: 90">Taxonómica</a>
                              </li>
                            }
                            <li>
                              <a className="uk-link-reset" href="#metodologia" data-uk-scroll="offset: 90">Método de muestreo</a>
                            </li>
                            {
                              data.bibliographicCitations && 
                              data.bibliographicCitations.length > 0 && <li>
                                <a className="uk-link-reset" href="#bibliografia" data-uk-scroll="offset: 90">Bibliografía</a>
                              </li>
                            }
                            <li>
                              <a className="uk-link-reset" href="#partes_asociadas" data-uk-scroll="offset: 90">Partes asociadas</a>
                            </li>
                            <li>
                              <a className="uk-link-reset" href="#citacion" data-uk-scroll="offset: 90">Citación</a>
                            </li>
                            <li>
                              <a className="uk-link-reset" href="#extensiones" data-uk-scroll="offset: 90">Extensiones</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="uk-width-3-4">
                        <DataSheet scroll="descripcion" title="Descripción">
                          <p>{data.eml.abstract}</p>
                        </DataSheet>
                        <DataSheet scroll="cobertura_temporal" title="Cobertura temporal" className="uk-margin-top">
                          {
                            data.eml_raw &&
                            this.temporalCoverage(data.eml_raw['eml:eml'].dataset.coverage.temporalCoverage)
                          }
                        </DataSheet>
                        { data.geographicCoverages &&
                        <div>
                        <DataSheet scroll="cobertura_geografica" title="Cobertura geográfica" className="uk-margin-top">
                          <div className="uk-margin-small-top">
                            <div>
                            {
                              data.geographicCoverages &&
                              Array.isArray(data.geographicCoverages) &&
                              data.geographicCoverages.map((value, key) => (<p key={key}>{value.description}</p>))
                            }
                            {
                              data.geographicCoverages &&
                              !Array.isArray(data.geographicCoverages) &&
                              <p>{data.geographicCoverages.description}</p>
                            }
                            {
                              data.geographicCoverages &&
                              !Array.isArray(data.geographicCoverages) &&
                              <p>{data.geographicCoverages.geographicDescription}</p>
                            }
                            </div>
                          </div>
                        </DataSheet>
                        <div className="uk-height-large">
                          <HumboldtMap zoom={5}><GeographicCoverages g={data.geographicCoverages} /></HumboldtMap>
                        </div>
                        </div>
                          }
                        {route(data, ["eml", "eml:eml", "dataset", "coverage", "taxonomicCoverage"])!=="" &&
                        <DataSheet scroll="cobertura_taxonomica" title="Cobertura taxonómica" className="uk-margin-top">
                          {this.taxonomyCoverage(data, ["eml", "eml:eml", "dataset", "coverage", "taxonomicCoverage"])}
                        </DataSheet>
                        }

                        <DataSheet scroll="metodologia" title="Metodología" className="uk-margin-top">
                          { data.eml_raw && data.eml_raw['eml:eml'].dataset.methods &&
                            <div className="uk-padding-small">

                              <h4 className="uk-text-bold uk-margin-small">Área de estudio</h4>
                              <p>{studyExtent}</p>

                              <h4 className="uk-text-bold uk-margin-small">Muestreo</h4>
                              <p>{samplingDescription}</p>

                              <h4 className="uk-text-bold uk-margin-small">Control de calidad</h4>
                              <p>{qualityControl}</p>

                              <h4 className="uk-text-bold uk-margin-small">Descripción de la metodología paso a paso</h4>
                                  {data.eml_raw['eml:eml'].dataset.methods.methodStep &&
                                  !Array.isArray(data.eml_raw['eml:eml'].dataset.methods.methodStep) &&
                                    <div className="uk-child-width-expand uk-grid-small" data-uk-grid="data-uk-grid">
                                      <div className="uk-width-auto">
                                        <span className="uk-badge uk-text-bold">*</span>
                                      </div>
                                      <div>{data.eml_raw['eml:eml'].dataset.methods.methodStep.description.para}</div>
                                    </div>
                                  }

                                  {data.eml_raw['eml:eml'].dataset.methods.methodStep &&
                                        Array.isArray(data.eml_raw['eml:eml'].dataset.methods.methodStep) &&
                                        <div>
                                        {data.eml_raw['eml:eml'].dataset.methods.methodStep.map((value, key) => {
                                          return <div className="uk-child-width-expand uk-grid-small" data-uk-grid="data-uk-grid" key={key}>
                                            <div className="uk-width-auto">
                                              <span className="uk-badge uk-text-bold">*</span>
                                            </div>
                                            <div>{value.description.para}</div>
                                          </div>
                                        })}
                                        </div>
                                  }
                            </div>
                          }
                        </DataSheet>
                          {
                            data.eml_raw && data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography &&
                            Array.isArray(data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography.citation.length) &&
                            <DataSheet scroll="bibliografia" title="Bibliografía" className="uk-margin-top">
                              <ol>
                                {data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography.citation.map((value, key) => {
                                
                                  if (typeof value === 'string'){
                                    return <li key={key}>{value}</li>
                                  }else{
                                    return <li key={key}>{value["#text"]}</li>
                                  }
                                })}
                              </ol>
                            </DataSheet>
                          }
                          {
                            data.eml_raw && data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography && 
                            !Array.isArray(data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography.citation.length) &&
                            <DataSheet scroll="bibliografia" title="Bibliografía" className="uk-margin-top">
                              <ol>
                                {()=>{
                                  if (typeof data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography.citation === 'string'){
                                    return <li>{data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography.citation}</li>
                                  }else{
                                    return <li>{data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography.citation["#text"]}</li>
                                  }
                                }}
                              </ol>
                            </DataSheet>
                          }
                        {data && data.eml_raw['eml:eml'].dataset.associatedParty &&
                        <DataSheet scroll="partes_asociadas" title="Partes asociadas" className="uk-margin-top">
                            <ContactsGrid data={data.eml_raw['eml:eml'].dataset.associatedParty} grid="1-2" />
                        </DataSheet>
                        }
                        {/*
                        <DataSheet scroll="registro_gbif" title="Registro en GBIF" className="uk-margin-top">
                          <table className="uk-table uk-table-small uk-margin-remove-bottom">
                            <tbody>
                              {
                                data.created && <tr>
                                  <td className="uk-text-bold">Fecha de registro</td>
                                  <td className="uk-table-expand">{new Date(data.created).toLocaleDateString()}</td>
                                </tr>
                              }
                              {
                                data.modified && <tr>
                                  <td className="uk-text-bold">Última edición</td>
                                  <td className="uk-table-expand">{new Date(data.modified).toLocaleDateString()}</td>
                                </tr>
                              }
                              {
                                data.pubDate && <tr>
                                  <td className="uk-text-bold">Fecha de publicación</td>
                                  <td className="uk-table-expand">{new Date(data.pubDate).toLocaleDateString()}</td>
                                </tr>
                              }
                              {
                                data.publishingOrganization && <tr>
                                  <td className="uk-text-bold">Host</td>
                                  <td className="uk-table-expand">{data.publishingOrganization.title}</td>
                                </tr>
                              }
                              <tr>
                                <td className="uk-text-bold">Instalación</td>
                                <td className="uk-table-expand">NO ENCONTRADO*</td>
                              </tr>
                              <tr>
                                <td className="uk-text-bold">Installation Contacts</td>
                                <td className="uk-table-expand">
                                  <ContactsGrid minified="minified" data={data.contacts} />
                                </td>
                              </tr>
                              <tr>
                                <td className="uk-text-bold">Endpoints</td>
                                <td className="uk-table-expand">
                                  {
                                    (data.endpoints).map((value, key) => (<p key={key}>
                                      <a className="uk-text-tertiary" href={value.url} target="_blank">{value.url}</a>
                                      ({value.type})</p>))
                                  }
                                </td>
                              </tr>
                              <tr>
                                <td className="uk-text-bold">Identifiers</td>
                                <td className="uk-table-expand">
                                  {this.handleIdentifiers(data.identifiers)}
                                </td>
                              </tr>
                              <tr>
                                <td className="uk-text-bold">Last succesful crawl</td>
                                <td className="uk-table-expand">NO ENCONTRADO*</td>
                              </tr>
                              <tr>
                                <td className="uk-text-bold">Crawling reasons</td>
                                <td className="uk-table-expand">NO ENCONTRADO*</td>
                              </tr>
                              <tr>
                                <td className="uk-text-bold">Crawls in total</td>
                                <td className="uk-table-expand">NO ENCONTRADO*</td>
                              </tr>
                            </tbody>
                          </table>
                        </DataSheet>
                        */}
                        <DataSheet scroll="citacion" title="Citación" className="uk-margin-top">
                          <p>{data.eml.citation}</p>
                        </DataSheet>
                        {/*
                        <DataSheet scroll="extensiones" title="Extensiones" className="uk-margin-top">
                          <h4 className="uk-text-bold uk-margin-small">Nombre de la extensión</h4>
                          <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt alias, quod quo illum nam modi harum libero asperiores soluta
                            non amet ducimus molestias illo quae, recusandae magnam iste? Deleniti, amet.
                            <br />  ESTA INFORMACION ESTA PENDIENTE DE RECIBIR*
                          </p>
                        </DataSheet>
                        */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="uk-container uk-container-expand uk-margin-top">
              {/*<p>
                <i>
                  <span className="uk-badge uk-background-quaternary" style={{ width: 12, height: 12, minWidth: 0 }} /> Portal datos
                </i>
                <i className="uk-margin-left">
                  <span className="uk-badge uk-background-tertiary" style={{ width: 12, height: 12, minWidth: 0 }} /> Colecciones en línea
                </i>
                <i className="uk-margin-left">
                  <span className="uk-badge" style={{ width: 12, height: 12, minWidth: 0 }} /> Catálogo de la biodiversidad
                </i>
              </p>*/}
              <SpeciesTable  occurrences={(number) => console.log(number)} busqueda={"gbifId="+data.gbifId} />
            </div>
          </ul>
        </div>) || <div className="uk-flex uk-flex-center uk-flex-middle uk-padding" data-uk-height-viewport="offset-top: true"><Loading /></div>
      }
    </GenericPage>)
  }
}

export default List;
