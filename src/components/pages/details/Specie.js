import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import GenericPage from '../../templates/GenericPage';
import Header from '../../organisms/Header';
import Footer from '../../organisms/Footer';
import Details from '../../molecules/Details';
import HumboldtMap from '../../molecules/HumboldtMap';
import * as OccurrenceService from '../../../services/OccurrenceService';
import Loading from '../../atoms/Loading';

import { Marker } from 'react-leaflet';

class Specie extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      imgs: null,
      imgf: null,
      isOpen: false,
      photoIndex: 0,
    }
  }

  componentWillMount() {
    if (this.props.match.params.id !== undefined) {
      OccurrenceService.getOccurrence(this.props.match.params.id).then(data => {
        this.setState({ occurrenceBasic: data })
      }).catch(err => {
        console.log(err)
      })

      OccurrenceService.getImagesList().then(data => {
        this.setState({ imgs: data.imagesBase, imgf: data.images })
      }).catch(err => {
        console.log(err)
      })
    }
  }

  render() {
    let basicInformation = false
    let o = {}
    let d = {}
    let p = {}
    if (this.state.occurrenceBasic) {
      o = this.state.occurrenceBasic.results.occurrence
      d = this.state.occurrenceBasic.results.dataset
      p = this.state.occurrenceBasic.results.organization
      basicInformation = {
        scientificName: o.scientificName,
        kingdom: o.kingdom,
        phylum: o.phylum,
        order: o.order,
        family: o.family,
        genus: o.genus,
        specificEpithet: o.specificEpithet,
        country: o.country,
        stateProvince: o.stateProvince,
        basisOfRecord: o.basisOfRecord,
        habitat: o.habitat,
        resourceName: d.title,
        providerName: p.title,
        decimalLongitude: o.decimalLongitude,
        decimalLatitude: o.decimalLatitude,
        description: d.description
      }
    }

    return (<GenericPage titlep="Búsqueda - Listas de Especies" header={<Header />} footer={<Footer />}>
      {
        (this.state.occurrenceBasic && <div className="uk-section uk-padding-remove-bottom">
          <div className="uk-flex uk-flex-center uk-grid-collapse" data-uk-grid="data-uk-grid">
            <div>
              <h4 className="uk-margin-left uk-heading-divider" style={{
                borderBottomColor: '#ff7847'
              }}>ESPÉCIMEN</h4>
            </div>
          </div>
          <div className="uk-flex uk-flex-center uk-grid-collapse uk-heading-divider uk-child-width-1-1" data-uk-grid="data-uk-grid">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l uk-text-center uk-margin-top uk-padding-remove">
              <h2>{basicInformation.scientificName}</h2>
            </div>
            <div className="uk-flex uk-margin-small-top uk-margin-small">
              <div>
                <h5>{basicInformation.kingdom}
                  <span data-uk-icon="icon: chevron-right; ratio: 0.9"></span>
                </h5>
              </div>
              <div>
                <h5>{basicInformation.phylum}
                  <span data-uk-icon="icon: chevron-right"></span>
                </h5>
              </div>
              <div>
                <h5>{basicInformation.order}
                  <span data-uk-icon="icon: chevron-right"></span>
                </h5>
              </div>
              <div>
                <h5>{basicInformation.family}
                  <span data-uk-icon="icon: chevron-right"></span>
                </h5>
              </div>
              <div>
                <h5>{basicInformation.genus}
                  <span data-uk-icon="icon: chevron-right"></span>
                </h5>
              </div>
              <div>
                <h5>{basicInformation.specificEpithet}</h5>
              </div>
            </div>
          </div>
          <div className="uk-section uk-section-small">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <div className="uk-child-width-1-2 uk-grid-small" data-uk-grid="data-uk-grid">
                <div>
                  <span className="uk-text-bold uk-margin-small-right">Nombre científico:</span>
                  <a href="">{basicInformation.scientificName}</a>
                </div>
                <div>
                  <span className="uk-text-bold uk-margin-small-right">Nombre de la lista:</span>
                  <Link to={`/dataset/${d.key}`}>{d.title}</Link>
                </div>
                <div>
                  <span className="uk-text-bold uk-margin-small-right">Localidad:</span>
                  {o.stateProvince}, {o.country}</div>
                <div>
                  <span className="uk-text-bold uk-margin-small-right">Publicador:</span>
                  <Link to={`/provider/${p.key}`}>{p.title}</Link>
                </div>
                <div>
                  <span className="uk-text-bold uk-margin-small-right">Ubicación:</span>
                  {o.stateProvince}, {o.country}
                </div>
              </div>
            </div>
          </div>
          {
            (o.decimalLatitude !== undefined && o.decimalLongitude !== undefined) &&
            <div className="uk-height-large">
              <HumboldtMap center={[o.decimalLatitude, o.decimalLongitude]} zoom={8}>
                <Marker position={{ lat: o.decimalLatitude, lon: o.decimalLongitude }} />
              </HumboldtMap>
            </div>
          }
          <div className="uk-section uk-section-small">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <div className="uk-flex uk-flex-column">
                <h3 className="uk-margin-left uk-text-bold">Sobre la lista</h3>
                <div className="uk-width-1-4 uk-width-small@s uk-background-primary" style={{
                  height: 2
                }}></div>
              </div>
              <div className="uk-column-1-2 uk-margin-top">
                <p>{basicInformation.description}</p>
              </div>
            </div>
          </div>
          <div className="uk-section uk-section-default uk-section-small">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <div className=" uk-child-width-1-1 uk-child-width-1-2@s" data-uk-grid="data-uk-grid">
                <Details title="Taxón">
                  <Details.Taxon data={o} />
                </Details>
                <Details title="Elemento de registro">
                  <Details.RegistrationElement data={o} />
                </Details>
                <Details title="Descripción del taxon">
                  <Details.TaxonDescription data={o} />
                </Details>
                <Details title="Perfil de especie">
                  <Details.SpeciesProfile data={o} />
                </Details>
                <Details title="Ejemplar de tipo">
                  <Details.ExemplaryType data={o} />
                </Details>
                <Details title="Nombres comunes">
                  <Details.CommonNames data={o} />
                </Details>
                <Details title="Distribución">
                  <Details.Distribution data={o} />
                </Details>
                <Details title="Referencias">
                  <Details.References data={o} />
                </Details>
              </div>
            </div>
          </div>
        </div>) || <div className="uk-flex uk-flex-center uk-flex-middle uk-padding" data-uk-height-viewport="offset-top: true"><Loading /></div>
      }
    </GenericPage>)
  }
}

export default Specie;
