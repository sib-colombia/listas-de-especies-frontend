import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import cx from 'classnames';
import { map, split, indexOf, join } from 'lodash';

import LogicPage from '../templates/LogicPage';
import Header from '../organisms/Header';
import Sidebar from '../organisms/Sidebar';
import Footer from '../organisms/Footer';
import Title from '../atoms/Title';
import ProvidersList from '../organisms/ProvidersList';
import ProvidersGrid from '../organisms/ProvidersGrid';
import Pagination from '../atoms/Pagination';
import * as ProviderService from '../../services/ProviderService';
import Filters from '../molecules/Filters';

class Providers extends Component {

  constructor(props) {
    super(props)
    this.state = {
      data: [],
      count: '',
      modified: false,
      kind: false,
      amount: null,
      search: props.location.search
    }

    this.count = []
    this.search = []
  }

  componentDidMount() {
    this.offsetResults(0, this.state.search)
    this.setState({ modified: true })
  }

  onChangePage(pageOfItems) {
    this.offsetResults(pageOfItems, this.state.search)
  }

  offsetResults(pageOfItems, search) {
    ProviderService.getProviderList(pageOfItems, search, this.state.orderBy)
      .then(data => {
        this.setState({ 
          data: data.aggregations.organizationId.buckets,
          count: data.aggregations.organizationId.doc_count_error_upper_bound,
        })
      })
      .catch(() => {
        this.setState({ data: [], count: 0 })        
      })
  }

  handleDisplay(kind) { this.setState({ kind }) }

  setCount(e, filter) {
    this.count[filter] = e.length;
    this.search[filter] = e;
    let stringQ = this.createQuery(this.search);
    window.history.replaceState('', 'Publicadores - Listas de Especies', `${this.props.location.pathname}${stringQ && '?' + stringQ}`)
    this.offsetResults(0, stringQ)

    this.setState({ amount: this.count, search: stringQ})
  }

  createQuery(data) {
    let search = this.props.location.search !== '' ? split((this.props.location.search).slice(1), '&') : [];
    search['query'] = []
    map(data, (value, key) => {
      map(value, (value1) => {
        const i = indexOf(search['query'], value1.id);
        if (i < 0) {
          search['query'].push(value1.id)
        }
      })
    })
    
    if (!this.state.modified && search['query'].length < 0)
      search['query'].unshift(join(search, '&'))

    return join(search['query'], '&');
  }

  activeFilters() {
    return this.props.location.search !== '' && split((this.props.location.search).slice(1), '&');
  }

  orderBy(orderBy) {
    this.setState({ orderBy }, () => this.offsetResults(0))
  }

  render() {
    return (
      <LogicPage
        titlep="Publicadores - Listas de Especies"
        headercustom={<Header withSidebar={Sidebar.id} />}
        footer={<Footer />}        
        sidebar={
          <Filters count={this.state.amount} search={this.state.search}>
            <Filters.ProviderName activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 0)} />
            <Filters.Project activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 1)} />
          </Filters>
        }
      >
        <div className="uk-section uk-section-small">
          <div className="uk-container uk-width-5-6@l uk-width-2-3@xl">
            <Title
              label={<span className="uk-text-bold">BÚSQUEDA POR PUBLICADORES</span>}
              icon={<span className="uk-text-primary" uk-icon="icon: triangle-right; ratio: 1.3" />}
              number={<NumberFormat value={this.state.count} displayType="text" thousandSeparator />}
              labelNumber="RESULTADOS"
              tag="h4"
            />
            <ul className="uk-margin-medium-top uk-flex-right" data-uk-tab="swiping: false;">
              <li className={cx({ 'uk-active': !this.state.kind })} onClick={() => this.handleDisplay(false)}><a title="Modo lista" data-uk-tooltip><span uk-icon="icon: list"></span></a></li>
              <li className={cx({ 'uk-active': this.state.kind })} onClick={() => this.handleDisplay(true)}><a title="Moto grilla" data-uk-tooltip><span uk-icon="icon: grid"></span></a></li>
              {/*<li>
                <a className="uk-text-right" title="Ordenar" data-uk-tooltip>A/Z</a>
                <div className="uk-padding-small" data-uk-dropdown="mode: hover">
                  <ul className="uk-nav uk-dropdown-nav">
                    <li onClick={() => this.orderBy('alpha')}><a>Alfabético</a></li>
                    <li onClick={() => this.orderBy('recent')}><a>Recientes</a></li>
                    <li onClick={() => this.orderBy('NRecords')}><a>Número de registros</a></li>
                  </ul>
                </div>
              </li>*/}
            </ul>
            {!this.state.kind ? <ProvidersList data={this.state.data} /> : <ProvidersGrid data={this.state.data} />}
          </div>
        </div>
        {(this.state.data && this.state.data.length > 0) &&
          <Pagination items={this.state.count} pageSize={5} onChangePage={(number) => this.onChangePage(number - 1)} />
        }
      </LogicPage>
    )
  }
}

export default Providers;
